//
//  Spot+CoreDataProperties.h
//  Spotix
//
//  Created by victor salazar on 11/11/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import "Spot+CoreDataClass.h"
NS_ASSUME_NONNULL_BEGIN
@interface Spot(CoreDataProperties)
+(NSFetchRequest<Spot *> *)fetchRequest;
@property(nullable, nonatomic, copy) NSString *imagePath;
@property(nullable, nonatomic, copy) NSString *spotPath;
@property(nullable, nonatomic, copy) NSNumber *type;
@property(nullable, nonatomic, copy) NSString *videoPath;
@property(nullable, nonatomic, copy) NSNumber *userId;
@property(nullable, nonatomic, copy) NSNumber *isPublic;
@property(nullable, nonatomic, copy) NSDate *creationDate;
@property(nullable, nonatomic, retain) Trackable *trackable;
@property(nullable, nonatomic, copy) NSString *selectedFriends;
@end
NS_ASSUME_NONNULL_END
