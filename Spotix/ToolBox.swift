//
//  ToolBox.swift
//  Tu Point
//
//  Created by victor salazar on 19/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import UIKit
class ToolBox:NSObject{
    class func getApplicationDocumentDirectory() -> String{
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    class func getImagesDirectory() -> String {
        return "\(self.getApplicationDocumentDirectory())/images"
    }
    class func getImageWith(path:String) -> UIImage {
        let imagesDirectoryPath = ToolBox.getImagesDirectory() as NSString
        let documentPath = imagesDirectoryPath.appendingPathComponent(path)
        return UIImage(contentsOfFile: documentPath)!
    }
    class func convertHexColorToColor(_ hexColor:String) -> UIColor {
        let strRed = hexColor.substring(to: hexColor.characters.index(hexColor.startIndex, offsetBy: 2))
        let strGreen = hexColor.substring(with: hexColor.characters.index(hexColor.startIndex, offsetBy: 2) ..< hexColor.characters.index(hexColor.startIndex, offsetBy: 4))
        let strBlue = hexColor.substring(from: hexColor.characters.index(hexColor.startIndex, offsetBy: 4))
        let red = self.convertHexStringToInt(strRed)
        let green = self.convertHexStringToInt(strGreen)
        let blue = self.convertHexStringToInt(strBlue)
        return UIColor(r: red, g: green, b: blue)
    }
    class func convertHexStringToInt(_ hexString:String) -> Int {
        let strHexCharacters = "0123456789abcdef"
        let strDecimal = hexString[hexString.startIndex]
        let strUnit = hexString[hexString.characters.index(hexString.startIndex, offsetBy: 1)]
        let decimalIndex = strHexCharacters.characters.index(of: strDecimal)!
        let unitIndex = strHexCharacters.characters.index(of: strUnit)!
        let decimal = strHexCharacters.characters.distance(from: strHexCharacters.startIndex, to: decimalIndex)
        let unit = strHexCharacters.characters.distance(from: strHexCharacters.startIndex, to: unitIndex)
        return decimal * 16 + unit
    }
    class func convertSquaredImage(_ image:UIImage) -> UIImage {
        let width = image.size.width
        let height = image.size.height
        let maxSide = max(width, height)
        let cant = floor(log2(maxSide))
        let finalSide = pow(2, cant)
        let finalSize = CGSize(width: finalSide, height: finalSide)
        let scaleFactor = finalSide / maxSide
        let finalWidth = floor(scaleFactor * width)
        let finalHeight = floor(scaleFactor * height)
        UIGraphicsBeginImageContext(finalSize)
        UIRectFill(CGRect(x: 0, y: 0, width: finalSide, height: finalSide))
        image.draw(in: CGRect(x: (finalSide - finalWidth) / 2 , y: (finalSide - finalHeight) / 2, width: finalWidth, height: finalHeight))
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return finalImage
    }
    
    class func showAlertWithTitle(_ title:String, withMessage message:String, withOkButtonTitle okButtonTitle:String = "OK", withOkHandler handler:((_ alertAction:UIAlertAction) -> Void)? = nil, inViewCont viewCont:UIViewController){
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: okButtonTitle, style: .default, handler: handler))
        viewCont.present(alertCont, animated: true, completion: nil)
    }
    class func showErrorConnectionInViewCont(_ viewCont:UIViewController){
        self.showAlertWithTitle("Alerta", withMessage: "Hubo error en la conexión.", inViewCont: viewCont)
    }
    class func showNoAccountMessageInViewCont(_ viewCont:UIViewController){
        let alertCont = UIAlertController(title: "Acceso no autorizado", message: "Si deseas ingresar a esta sección debes tener una cuenta activa.", preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "Seguir navegando", style: .default, handler: nil))
        alertCont.addAction(UIAlertAction(title: "Crear cuenta", style: .default, handler: { (action:UIAlertAction) in
            viewCont.dismiss(animated: true, completion: nil)
        }))
        viewCont.present(alertCont, animated: true, completion: nil)
    }
    class func convertDictionaryToPostParams(_ dictionary:Dictionary<String, String>) -> String {
        var arrParams = Array<String>()
        for (key, value) in dictionary {
            arrParams.append("\(key)=\(value)")
        }
        return arrParams.joined(separator: "&")
    }
}
