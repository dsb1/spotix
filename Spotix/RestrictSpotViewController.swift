//
//  RestrictSpotViewController.swift
//  Spotix
//
//  Created by victor salazar on 11/21/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class RestrictSpotViewController:UIViewController{
    //MARK: - Variables
    var trackable:Trackable!
    //MARK: - IBOutlet
    @IBOutlet weak var backBtn:UIButton!
    @IBOutlet weak var publicBtn:UIButton!
    @IBOutlet weak var friendsBtn:UIButton!
    @IBOutlet weak var selectBtn:UIButton!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        backBtn.setImage(UIImage(named: "Back")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
        publicBtn.layer.borderColor = UIColor(rgb: 132).cgColor
        friendsBtn.layer.borderColor = UIColor(rgb: 132).cgColor
        updateViews()
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? FriendsViewController {
            viewCont.spot = trackable.actualSpot!
        }
    }
    //MARK: - IBAction
    @IBAction func back(){
        dismiss(animated: true, completion: nil)
    }
    @IBAction func makePublicSpot(){
        if !trackable.actualSpot!.isPublic!.boolValue {
            Spot.make(trackable.actualSpot!, public: true)
            updateViews()
        }
    }
    @IBAction func makeFriendsSpot(){
        if trackable.actualSpot!.isPublic!.boolValue {
            Spot.make(trackable.actualSpot!, public: false)
            updateViews()
        }
    }
    //MARK: - Auxiliar
    func updateViews(){
        if trackable.actualSpot!.isPublic!.boolValue {
            publicBtn.backgroundColor = UIColor(rgb: 132)
            publicBtn.setImage(UIImage(named: "Check"), for: .normal)
            friendsBtn.backgroundColor = UIColor.clear
            friendsBtn.setImage(nil, for: .normal)
            selectBtn.isEnabled = false
            selectBtn.layer.borderColor = UIColor.lightGray.cgColor
        }else{
            publicBtn.backgroundColor = UIColor.clear
            publicBtn.setImage(nil, for: .normal)
            friendsBtn.backgroundColor = UIColor(rgb: 132)
            friendsBtn.setImage(UIImage(named: "Check"), for: .normal)
            selectBtn.isEnabled = true
            selectBtn.layer.borderColor = UIColor.black.cgColor
        }
    }
}
