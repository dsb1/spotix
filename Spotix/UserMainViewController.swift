//
//  UserMainViewController.swift
//  Spotix
//
//  Created by victor salazar on 8/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class UserMainViewController:UIViewController{
    //MARK: - Variables
    var currentOption = -1
    //MARK: - IBOutlet
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var menuView:UIView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
    }
    //MARK: - Auxiliar
    func showMenu(){
        menuView.isHidden = false
        menuView.transform = CGAffineTransform(translationX: -view.frame.width, y: 0)
        UIView.animate(withDuration: 0.5, animations: {
            self.menuView.transform = CGAffineTransform.identity
        })
    }
    func hideMenu(){
        UIView.animate(withDuration: 0.5, animations: {
            self.menuView.transform = CGAffineTransform(translationX: -self.view.frame.width, y: 0)
        }, completion: {(b:Bool) in
            self.menuView.transform = CGAffineTransform.identity
            self.menuView.isHidden = true
        })
    }
    func selectOption(option:Int){
        if option == -1 {
            currentOption = option
            loadViewContWithCurrentOption()
            return
        }
        hideMenu()
        if currentOption != option {
            currentOption = option
            loadViewContWithCurrentOption()
        }
    }
    func loadViewContWithCurrentOption(){
        let identifier = ["userNavCont", "placedElementsViewCont", "mapOfPlacesViewCont", "placedElementsViewCont"][currentOption + 1]
        for childViewCont in childViewControllers {
            if !(childViewCont is MenuViewController) {
                childViewCont.view.removeFromSuperview()
                childViewCont.removeFromParentViewController()
            }
        }
        let viewCont = storyboard?.instantiateViewController(withIdentifier: identifier)
        if currentOption % 2 == 0 {
            if let placedElementViewCont = viewCont as? PlacedDiscoverElementsViewController {
                placedElementViewCont.loadMySpots = currentOption == 0
            }
        }
        viewCont?.view.frame = contentView.frame
        contentView.addSubview(viewCont!.view)
        self.addChildViewController(viewCont!)
    }
}
