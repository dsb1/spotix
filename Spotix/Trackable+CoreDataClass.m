//
//  Trackable+CoreDataClass.m
//  Spotix
//
//  Created by Victor Salazar on 11/9/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import "Trackable+CoreDataClass.h"
#import <Photos/Photos.h>
#import <Spotix-Swift.h>
@implementation Trackable
+(NSArray *)getTrackables{
    NSManagedObjectContext *context = [AppDelegate getMOC];
    NSFetchRequest<Trackable *> *fecthRequest = [Trackable fetchRequest];
    NSArray *arrResults = [context executeFetchRequest:fecthRequest error:nil];
    if(arrResults == nil){
        return @[];
    }
    return arrResults;
}
+(Trackable *)createTrackable:(NSDictionary *)dicTrackable{
    NSManagedObjectContext *context = [AppDelegate getMOC];
    Trackable *newTrackable = [NSEntityDescription insertNewObjectForEntityForName:@"Trackable" inManagedObjectContext:context];
    newTrackable.fileName = [dicTrackable objectForKey:@"fileName"];
    newTrackable.latitude = [dicTrackable objectForKey:@"latitude"];
    newTrackable.longitude = [dicTrackable objectForKey:@"longitude"];
    newTrackable.locationName = [dicTrackable objectForKey:@"locationName"];
    NSError *error;
    [context save:&error];
    NSLog(@"%s error: %@", __func__, error);
    return newTrackable;
}
@end
