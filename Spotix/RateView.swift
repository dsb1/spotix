//
//  RateView.swift
//  RateView
//
//  Created by Victor Salazar on 15/10/15.
//  Copyright © 2015 Victor Salazar. All rights reserved.
//
import UIKit
class RateView:UIView{
    var arrStarView:Array<StarView> = []
    var rate:CGFloat{
        didSet{
            for i in 0 ... 4 {
                let starView = arrStarView[i]
                let starRate = rate - CGFloat(i)
                starView.value = starRate > 1 ? 1 : starRate
                starView.setNeedsDisplay()
            }
        }
    }
    var spaceBetweenStar:CGFloat = 5
    required init?(coder aDecoder:NSCoder){
        rate = 5.0
        super.init(coder:aDecoder)
    }
    override func layoutSubviews(){
        super.layoutSubviews()
        let size = self.frame.height
        for i in 0 ... 4 {
            let star = StarView(frame: CGRect(x: CGFloat(i) * (size + spaceBetweenStar), y: 0, width: size, height: size))
            let starRate = rate - CGFloat(i)
            star.value = starRate > 1 ? 1 : starRate
            arrStarView.append(star)
            self.addSubview(star)
        }
    }
    
}
