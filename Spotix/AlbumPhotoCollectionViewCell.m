//
//  AlbumPhotoCollectionViewCell.m
//  Spotix
//
//  Created by victor salazar on 10/4/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import "AlbumPhotoCollectionViewCell.h"
@implementation AlbumPhotoCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        self.photoImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.photoImgView.backgroundColor = [UIColor grayColor];
        self.photoImgView.contentMode = UIViewContentModeScaleAspectFill;
        self.photoImgView.clipsToBounds = true;
        [self addSubview:self.photoImgView];
    }
    return self;
}
-(void)updateVisibilitySelectionView:(BOOL)show{
    if(show){
        if(selectionView == nil){
            selectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            selectionView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
            self.editBtn = [[UIButton alloc] initWithFrame:CGRectMake(35, 75, 50, 50)];
            [self.editBtn setImage:[UIImage imageNamed:@"ModifiedPhoto"] forState:UIControlStateNormal];
            [selectionView addSubview:self.editBtn];
            self.acceptBtn = [[UIButton alloc] initWithFrame:CGRectMake(115, 75, 50, 50)];
            [self.acceptBtn setImage:[UIImage imageNamed:@"AcceptPhoto"] forState:UIControlStateNormal];
            [selectionView addSubview:self.acceptBtn];
            [self addSubview:selectionView];
        }else{
            selectionView.hidden = false;
        }
    }else{
        if(selectionView != nil){
            selectionView.hidden = true;
        }
    }
}
@end
