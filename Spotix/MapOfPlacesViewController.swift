//
//  MapOfPlacesViewController.swift
//  Spotix
//
//  Created by victor salazar on 11/8/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
import GoogleMaps
class MapOfPlacesViewController:UIViewController, GMSMapViewDelegate {
    //MARK: - Variables
    var userMainViewCont:UserMainViewController!
    var arrTrackables = Trackable.getTrackables() as! Array<Trackable>
    //MARK: - IBOutlet
    @IBOutlet weak var closeBtn:UIButton!
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var subtitleLbl:UILabel!
    @IBOutlet weak var bottomImgView:UIImageView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        closeBtn.setImage(UIImage(named: "Close")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
        let camera = GMSCameraPosition.camera(withLatitude: -9.1700164, longitude: -74.6487896, zoom: 5)
        mapView.camera = camera
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        mapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
        for trackable in arrTrackables {
            let position = CLLocationCoordinate2D.init(latitude: trackable.latitude!.doubleValue, longitude: trackable.longitude!.doubleValue)
            let marker = GMSMarker(position: position)
            marker.userData = trackable
            marker.map = mapView
        }
    }
    override func viewDidAppear(_ animated:Bool){
        super.viewDidAppear(animated)
        userMainViewCont = self.parent as! UserMainViewController
    }
    //MARK: - IBAction
    @IBAction func close(){
        userMainViewCont.selectOption(option: -1)
    }
    @IBAction func showCurrentLocation(){
        let camera = GMSCameraPosition.camera(withTarget: mapView.myLocation!.coordinate, zoom: 15)
        mapView.animate(to: camera)
    }
    //MARK: - MapView
    func mapView(_ mapView:GMSMapView, markerInfoWindow marker:GMSMarker) -> UIView? {
        let finalView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 110))
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint.zero)
        bezierPath.addLine(to: CGPoint(x: 10, y: 0))
        bezierPath.addLine(to: CGPoint(x: 5, y: 8.66))
        bezierPath.addLine(to: CGPoint.zero)
        let whiteTriangle = CAShapeLayer()
        whiteTriangle.path = bezierPath.cgPath
        whiteTriangle.fillColor = UIColor.white.cgColor
        whiteTriangle.frame = CGRect(x: 45, y: 100, width: 10, height: 10)
        finalView.layer.addSublayer(whiteTriangle)
        let viewTemp = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        viewTemp.backgroundColor = UIColor.white
        
        let spotImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        spotImgView.contentMode = .scaleAspectFit
        var trackableSpoted = false
        if let trackable = marker.userData as? Trackable {
            if let spot = trackable.actualSpot {
                spotImgView.image = ToolBox.getImageWith(path: spot.spotPath!)
                spotImgView.contentMode = .scaleAspectFit
                trackableSpoted = true
                if spot.isPublic!.boolValue {
                    titleLbl.text = "Spot"
                    subtitleLbl.text = "Check this spot, look awesome"
                    bottomImgView.image = UIImage(named: "PublicSpot")
                }else{
                    titleLbl.text = "Spot!"
                    subtitleLbl.text = "This spot is just for friends. Try another one."
                    bottomImgView.image = UIImage(named: "NoPublicSpot")
                }
            }
        }
        if !trackableSpoted {
            spotImgView.contentMode = .center
            spotImgView.image = UIImage(named: "PublicSpot")
            titleLbl.text = "Free space"
            subtitleLbl.text = "Put here: Note, Image or Video"
            bottomImgView.image = UIImage(named: "PublicSpot")
        }
        viewTemp.addSubview(spotImgView)
        finalView.addSubview(viewTemp)
        
        return finalView
    }
    //MARK: - KVO
    override func observeValue(forKeyPath keyPath:String?, of object:Any?, change:[NSKeyValueChangeKey:Any]?, context: UnsafeMutableRawPointer?){
        if keyPath == "myLocation" {
            mapView.removeObserver(self, forKeyPath: "myLocation")
            showCurrentLocation()
        }
    }
}

