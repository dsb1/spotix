//
//  PlacedElementCollectionViewCell.swift
//  Spotix
//
//  Created by victor salazar on 11/10/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class ElementCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var elementImgView:UIImageView!
    @IBOutlet weak var elementTypeLbl:UILabel!
    @IBOutlet weak var locationNameLbl:UILabel!
}
