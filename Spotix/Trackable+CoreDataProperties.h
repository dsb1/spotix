//
//  Trackable+CoreDataProperties.h
//  Spotix
//
//  Created by Victor Salazar on 11/9/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import "Trackable+CoreDataClass.h"
NS_ASSUME_NONNULL_BEGIN
@interface Trackable (CoreDataProperties)
+(NSFetchRequest<Trackable *> *)fetchRequest;
@property(nullable, nonatomic, copy) NSString *fileName;
@property(nullable, nonatomic, copy) NSNumber *latitude;
@property(nullable, nonatomic, copy) NSNumber *longitude;
@property(nullable, nonatomic, copy) NSString *locationName;
@property(nullable, nonatomic, retain) Spot *actualSpot;
@end
NS_ASSUME_NONNULL_END
