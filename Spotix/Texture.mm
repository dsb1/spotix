/*===============================================================================
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/
#import "Texture.h"
@implementation Texture
#pragma mark - Lifecycle
-(void)dealloc{
    if(_pngData){
        delete[] _pngData;
    }
}
-(id)initWithImage:(UIImage *)image{
    if(self = [super init]){
        CGImageRef cgImage = image.CGImage;
        _width = (int)CGImageGetWidth(cgImage);
        _height = (int)CGImageGetHeight(cgImage);
        _channels = (int)CGImageGetBitsPerPixel(cgImage) / CGImageGetBitsPerComponent(cgImage);
        CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
        [self copyImageDataForOpenGL:imageData];
        CFRelease(imageData);
    }
    return self;
}
#pragma mark - Private methods
-(BOOL)copyImageDataForOpenGL:(CFDataRef)imageData{
    if(_pngData){
        delete[] _pngData;
    }
    _pngData = new unsigned char[_width * _height * _channels];
    const int rowSize = _width * _channels;
    const unsigned char* pixels = (unsigned char*)CFDataGetBytePtr(imageData);
    for (int i = 0; i < _height; ++i) {
        memcpy(_pngData + rowSize * i, pixels + rowSize * (_height - 1 - i), _width * _channels);
    }
    return YES;
}
@end
