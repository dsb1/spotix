//
//  ImageElementsViewController.swift
//  Spotix
//
//  Created by victor salazar on 10/26/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
import Photos
import CoreGraphics
class ImageElementsViewController:UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CAAnimationDelegate {
    //MARK: - Variables
    var arrPhotos:PHFetchResult<PHAsset>!
    let imageManager = PHCachingImageManager()
    var cellWidth:CGFloat = 0
    let circleLayer = CAShapeLayer()
    var loadingImages:Bool = false
    var isOpenCustomOptions = false
    var bigCirclePath:CGPath?, tinyCirclePath:CGPath?
    var actualTrackable:Trackable!
    var videoImage:UIImage!
    //MARK: - IBOutlet
    @IBOutlet weak var backBtn:UIButton!
    @IBOutlet weak var cameraBtn:UIButton!
    @IBOutlet weak var showingOptionsView:UIView!
    @IBOutlet weak var customOptionsView:UIView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions.predicate = NSPredicate(format: "mediaType == %d", (self.loadingImages ? PHAssetMediaType.image.rawValue : PHAssetMediaType.video.rawValue))
        arrPhotos = PHAsset.fetchAssets(with: fetchOptions)
        tinyCirclePath = UIBezierPath(ovalIn: CGRect(x: view.frame.width - 85, y: view.frame.height - 90, width: 60, height: 60)).cgPath
        circleLayer.path = tinyCirclePath
        circleLayer.fillColor = UIColor(r: 0, g: 215, b: 208).cgColor
        showingOptionsView.layer.insertSublayer(circleLayer, at: 0)
        backBtn.setImage(UIImage(named: "Back")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
    }
    override func viewDidDisappear(_ animated:Bool){
        super.viewDidDisappear(animated)
        if isOpenCustomOptions {
            closeCustomOptions()
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? CapturePhotoVideoViewController {
            let option = sender as! Int
            viewCont.isCapturingPhoto = option == 0
            viewCont.actualTrackable = actualTrackable
            viewCont.videoImage = videoImage
        }
    }
    //MARK: - IBAction
    @IBAction func back(){
        dismiss(animated: true){}
    }
    @IBAction func showOptions(){
        isOpenCustomOptions = true
        cameraBtn.isHidden = true
        if bigCirclePath == nil {
            let x = view.frame.width - 55
            let y = view.frame.height - 60
            let r = ceil(sqrt(pow(x, 2) + pow(y, 2)))
            bigCirclePath = UIBezierPath(ovalIn: CGRect(x: x - r, y: y - r, width: 2 * r, height: 2 * r)).cgPath
        }
        circleLayer.path = bigCirclePath
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = tinyCirclePath
        animation.toValue = bigCirclePath
        animation.duration = 0.25
        animation.delegate = self
        circleLayer.add(animation, forKey: "fullPath")
    }
    @IBAction func closeCustomOptions(){
        isOpenCustomOptions = false
        customOptionsView.isHidden = true
        circleLayer.path = tinyCirclePath
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = bigCirclePath
        animation.toValue = tinyCirclePath
        animation.duration = 0.25
        animation.delegate = self
        circleLayer.add(animation, forKey: "fullPath")
    }
    @IBAction func takePhoto(){
        performSegue(withIdentifier: "showCapturePhotoVideo", sender: 0)
    }
    @IBAction func makeVideo(){
        performSegue(withIdentifier: "showCapturePhotoVideo", sender: 1)
    }
    //MARK: - CollectionView
    func collectionView(_ collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        return arrPhotos.count
    }
    func collectionView(_ collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath:IndexPath) -> CGSize {
        cellWidth = floor((view.frame.width - 50) / 2)
        return CGSize(width: cellWidth, height: cellWidth)
    }
    func collectionView(_ collectionView:UICollectionView, cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "elementCell", for: indexPath) as! ImageElementCollectionViewCell
        let asset = arrPhotos[(indexPath as NSIndexPath).item]
        cell.elementImgView.image = nil
        cell.playImgView.isHidden = (asset.mediaType != .video)
        imageManager.requestImage(for: asset, targetSize: CGSize(width: cellWidth, height: cellWidth), contentMode: .aspectFit, options: nil){(image:UIImage?, info:[AnyHashable: Any]?) in
            cell.elementImgView.image = image
        }
        return cell
    }
    func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath:IndexPath){
        let cell = collectionView.cellForItem(at: indexPath) as! ImageElementCollectionViewCell
        let selectedImage = cell.elementImgView.image!
        let finalImage = ToolBox.convertSquaredImage(selectedImage)
        let type = loadingImages ? 1: 2
        if type == 0 {
            let phAsset = arrPhotos[indexPath.row]
            imageManager.requestAVAsset(forVideo: phAsset, options: nil, resultHandler:{(asset:AVAsset?, audioMix:AVAudioMix?, info:[AnyHashable:Any]?) in
                let urlAsset = asset as! AVURLAsset
                Spot.creatSpot(with: finalImage, withVideoImage: self.videoImage, withType: Int32(type), withVideoPath: urlAsset.url.path, with: self.actualTrackable, isPublic: true, withUserId: 1)
                self.dismiss(animated: true, completion: nil)
            })
        }else{
            Spot.creatSpot(with: finalImage, withVideoImage: videoImage, withType: Int32(type), withVideoPath: nil, with: actualTrackable, isPublic: true, withUserId: 1)
            dismiss(animated: true, completion: nil)
        }
    }
    //MARK: - Animation
    func animationDidStop(_ anim:CAAnimation, finished flag:Bool){
        if isOpenCustomOptions {
            customOptionsView.isHidden = false
            showingOptionsView.isUserInteractionEnabled = true
        }else{
            cameraBtn.isHidden = false
            showingOptionsView.isUserInteractionEnabled = false
        }
    }
}
