//
//  Trackable+CoreDataProperties.m
//  Spotix
//
//  Created by Victor Salazar on 11/9/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import "Trackable+CoreDataProperties.h"
@implementation Trackable(CoreDataProperties)
+(NSFetchRequest<Trackable *> *)fetchRequest{
	return [[NSFetchRequest alloc] initWithEntityName:@"Trackable"];
}
@dynamic fileName;
@dynamic latitude;
@dynamic longitude;
@dynamic locationName;
@dynamic actualSpot;
@end
