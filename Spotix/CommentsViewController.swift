//
//  CommentsViewController.swift
//  Spotix
//
//  Created by victor salazar on 11/21/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit

class Comment{
    var userName:String
    var comment:String
    init(_ nUserName:String, _ nComment:String){
        userName = nUserName
        comment = nComment
    }
}

class CommentsViewController:UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    //MARK: - Variables
    var arrComments:Array<Comment> = []
    //MARK: - IBOutlet
    @IBOutlet weak var backBtn:UIButton!
    @IBOutlet weak var commentsTableView:UITableView!
    @IBOutlet weak var commentTxtFld:UITextField!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var sendBtn:UIButton!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        commentsTableView.estimatedRowHeight = 60
        commentsTableView.rowHeight = UITableViewAutomaticDimension
        backBtn.setImage(UIImage(named: "Back")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    //MARK: - IBAction
    @IBAction func back(){
        dismiss(animated: true, completion: nil)
    }
    @IBAction func sendComment(){
        let newComment = Comment("Victor Salazar", commentTxtFld.text!)
        arrComments.append(newComment)
        commentTxtFld.text = ""
        commentTxtFld.resignFirstResponder()
        sendBtn.isEnabled = false
        commentsTableView.reloadData()
    }
    @IBAction func dismissKeyboard(){
        commentTxtFld.resignFirstResponder()
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrComments.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
        let comment = arrComments[indexPath.row]
        cell.userNameLbl.text = comment.userName
        cell.commentLbl.text = comment.comment
        return cell
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:NSNotification){
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let newContentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        commentsTableView.contentInset = newContentInset
        commentsTableView.scrollIndicatorInsets = newContentInset
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomView.transform = CGAffineTransform(translationX: 0, y: -keyboardHeight)
        })
    }
    func hideKeyboard(_ notification:NSNotification){
        let newContentInset = UIEdgeInsets.zero
        commentsTableView.contentInset = newContentInset
        commentsTableView.scrollIndicatorInsets = newContentInset
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomView.transform = CGAffineTransform.identity
        })
    }
    //MARK: - TextFld
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        sendBtn.isEnabled = (textField.text! as NSString).replacingCharacters(in: range, with: string).characters.count > 0
        return true
    }
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
}
