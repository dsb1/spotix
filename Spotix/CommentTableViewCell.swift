//
//  CommentTableViewCell.swift
//  Spotix
//
//  Created by victor salazar on 11/21/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class CommentTableViewCell:UITableViewCell{
    @IBOutlet weak var userNameLbl:UILabel!
    @IBOutlet weak var commentLbl:UILabel!
}
