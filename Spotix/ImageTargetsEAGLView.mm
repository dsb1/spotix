/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <sys/time.h>
#import <Vuforia/Vuforia.h>
#import <Vuforia/State.h>
#import <Vuforia/Tool.h>
#import <Vuforia/Renderer.h>
#import <Vuforia/TrackableResult.h>
#import <Vuforia/Tracker.h>
#import <Vuforia/ImageTarget.h>
#import <Vuforia/VideoBackgroundConfig.h>
#import <Vuforia/CameraCalibration.h>
#import "SampleApplicationUtils.h"
#import "SampleApplicationShaderUtils.h"
#import "ImageTargetsEAGLView.h"
#import "Trackable+CoreDataClass.h"
#import "Spot+CoreDataClass.h"
#import <Photos/Photos.h>
#import <GoogleMaps/GoogleMaps.h>
#import <Spotix-Swift.h>

#define ObjectScaleNormal 5.0f
#define ObjectSize 200.0f

@interface ImageTargetsEAGLView (PrivateMethods)
-(void)initShaders;
-(void)createFramebuffer;
-(void)deleteFramebuffer;
-(void)setFramebuffer;
-(BOOL)presentFramebuffer;
@end
@implementation ImageTargetsEAGLView
@synthesize vapp = vapp;
+(Class)layerClass{
    return [CAEAGLLayer class];
}
#pragma mark - Lifecycle
-(id)initWithFrame:(CGRect)frame appSession:(SampleApplicationSession *)app{
    if(self = [super initWithFrame:frame]){
        vapp = app;
        if([vapp isRetinaDisplay]){
            [self setContentScaleFactor:[UIScreen mainScreen].nativeScale];
        }
        context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        if(context != [EAGLContext currentContext]) {
            [EAGLContext setCurrentContext:context];
        }
        
        currentResourceImgView = [UIImageView new];
        currentResourceImgView.contentMode = UIViewContentModeCenter;
        currentResourceImgView.userInteractionEnabled = true;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomInOutImage)];
        [currentResourceImgView addGestureRecognizer:tapGesture];
        [self addSubview:currentResourceImgView];
        
        sampleAppRenderer = [[SampleAppRenderer alloc] initWithSampleAppRendererControl:self deviceMode:Vuforia::Device::MODE_AR stereo:false];
        [sampleAppRenderer initRendering];
        
        arrTrackables = [Trackable getTrackables];
    }
    return self;
}
-(void)dealloc{
    [self deleteFramebuffer];
    if ([EAGLContext currentContext] == context) {
        [EAGLContext setCurrentContext:nil];
    }
    actualTexture = nil;
}
-(void)finishOpenGLESCommands{
    if(context){
        [EAGLContext setCurrentContext:context];
        glFinish();
    }
}
-(void)freeOpenGLESResources{
    [self deleteFramebuffer];
    glFinish();
}
#pragma mark - UIGLViewProtocol methods
-(void)renderFrameVuforia{
    if(!vapp.cameraIsStarted){
        return;
    }
    [sampleAppRenderer renderFrameVuforia];
}
-(void)renderFrameWithState:(const Vuforia::State &)state projectMatrix:(Vuforia::Matrix44F &)projectionMatrix{
    [self setFramebuffer];
    [sampleAppRenderer renderVideoBackground];
    bool showingMask = false;
    currentResourceImgView.hidden = true;
    for(int i = 0; i < state.getNumTrackableResults(); ++i){
        const Vuforia::TrackableResult *result = state.getTrackableResult(i);
        const Vuforia::Trackable *trackable = &result->getTrackable();
        int index = 0;
        for(int i = 0; i < state.getNumTrackables(); i++) {
            if(state.getTrackable(i) == trackable){
                index = i;
                break;
            }
        }
        Trackable *newTrackable = [arrTrackables objectAtIndex:index];
        if(self.actualTrackable != newTrackable){
            [self changeActualTrackable:newTrackable];
        }
        currentResourceImgView.hidden = false;
        showingMask = didLoadCustomImage;
    
        const Vuforia::Matrix34F &pose = result->getPose();
        Vuforia::CameraDevice &cameraDevice = Vuforia::CameraDevice::getInstance();
        const Vuforia::CameraCalibration &cameraCalibration = cameraDevice.getCameraCalibration();
        Vuforia::Vec2F cameraPointCenter = Vuforia::Tool::projectPoint(cameraCalibration, pose, Vuforia::Vec3F(0, 0, 0));
        Vuforia::Vec2F cameraPointCenterLeft = Vuforia::Tool::projectPoint(cameraCalibration, pose, Vuforia::Vec3F(250, 0, 0));
        
        Vuforia::VideoMode videoMode = cameraDevice.getVideoMode(Vuforia::CameraDevice::MODE_DEFAULT);
        float wScale = videoMode.mHeight / self.frame.size.width;
        float hScale = videoMode.mWidth / self.frame.size.height;
        float finalScale = MAX(wScale, hScale);
        float centerX = (videoMode.mHeight - cameraPointCenter.data[1]) / finalScale;
        float centerY = cameraPointCenter.data[0] / finalScale;
        
        float centerLeftX = (videoMode.mHeight = cameraPointCenterLeft.data[1]) / finalScale;
        float centerLeftY = cameraPointCenterLeft.data[0] / finalScale;
        
        CGFloat slope = (centerLeftY - centerY) / (centerLeftX - centerX);
        CGFloat angle = atan(slope) * -1.0;
        
        currentResourceImgView.frame = CGRectMake(centerX - 25, centerY - 25, 50, 50);
        
        UIView *rotatedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        rotatedView.transform = CGAffineTransformMakeRotation(angle);
        
        CGSize rotatedSize = rotatedView.frame.size;
        
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, [[UIScreen mainScreen] scale]);
        CGContextRef bitmap = UIGraphicsGetCurrentContext();
        
        CGContextTranslateCTM(bitmap, rotatedSize.width / 2, rotatedSize.height / 2);
        
        CGContextRotateCTM(bitmap, angle);
        
        CGContextScaleCTM(bitmap, 50.0 / currentImage.size.width, -50.0 / currentImage.size.width);
        CGContextDrawImage(bitmap, CGRectMake(-currentImage.size.width / 2, -currentImage.size.height / 2 , currentImage.size.width, currentImage.size.height), currentImage.CGImage);
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        currentResourceImgView.image = newImage;
    }
    if(self.delegate){
        [self.delegate imageTargetShowingMask:showingMask];
    }
    [self presentFramebuffer];
}
#pragma mark - OpenGL ES management
-(void)createFramebuffer{
    if(context){
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
        GLint framebufferWidth;
        GLint framebufferHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &framebufferWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &framebufferHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, framebufferWidth, framebufferHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    }
}
-(void)deleteFramebuffer{
    if(context){
        [EAGLContext setCurrentContext:context];
        if(defaultFramebuffer){
            glDeleteFramebuffers(1, &defaultFramebuffer);
            defaultFramebuffer = 0;
        }
        if(colorRenderbuffer){
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            colorRenderbuffer = 0;
        }
        if(depthRenderbuffer){
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            depthRenderbuffer = 0;
        }
    }
}
-(void)setFramebuffer{
    if(context != [EAGLContext currentContext]){
        [EAGLContext setCurrentContext:context];
    }
    if(!defaultFramebuffer){
        [self performSelectorOnMainThread:@selector(createFramebuffer) withObject:self waitUntilDone:YES];
    }
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
}
-(BOOL)presentFramebuffer{
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    return [context presentRenderbuffer:GL_RENDERBUFFER];
}
-(void)loadTextureWithImage:(UIImage *)image{
    currentResourceImgView.image = image;
    didLoadCustomImage = true;
}
-(void)clearFrameBuffer{
    glClearColor(1, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
#pragma mark - IBAction
-(void)zoomInOutImage{
    if(didLoadCustomImage){
        zoomIn = true;
        if(zoomIn){
            if(selectedView == nil){
                selectedView = [[UIView alloc] initWithFrame:self.frame];
                selectedView.backgroundColor = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:0.7];
                [self addSubview:selectedView];
                UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                [closeBtn setFrame:CGRectMake(20, 20, 40, 40)];
                [closeBtn setTitle:@"X" forState:UIControlStateNormal];
                [closeBtn setBackgroundColor:[UIColor redColor]];
                closeBtn.layer.cornerRadius = 20;
                [closeBtn addTarget:self action:@selector(dismissSelectedView) forControlEvents:UIControlEventTouchUpInside];
                [selectedView addSubview:closeBtn];
            }
            selectedView.hidden = true;
            [self bringSubviewToFront:currentResourceImgView];
            [vapp pauseAR:nil];
            currentResourceImgView.contentMode = UIViewContentModeScaleAspectFit;
            currentResourceImgView.image = [ToolBox getImageWithPath:self.actualTrackable.actualSpot.spotPath];
            lastFrame = currentResourceImgView.frame;
            CGRect finalFrame = CGRectMake(0, (self.frame.size.height - self.frame.size.width) / 2, self.frame.size.width, self.frame.size.width);
            [UIView animateWithDuration:0.5 animations:^{
                currentResourceImgView.frame = finalFrame;
            } completion:^(BOOL finished){
                selectedView.hidden = false;
                if(self.actualTrackable.actualSpot.type.intValue == 1){
                    playBtn = [[UIButton alloc] initWithFrame:finalFrame];
                    [playBtn setImage:[UIImage imageNamed:@"Play"] forState:UIControlStateNormal];
                    [playBtn addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
                    [self addSubview:playBtn];
                }
            }];
        }
    }else{
        lastFrame = currentResourceImgView.frame;
        [self.delegate imageTargetShouldLoadResource];
    }
}
-(void)dismissSelectedView{
    zoomIn = false;
    selectedView.hidden = true;
    [playBtn removeFromSuperview];
    playBtn = nil;
    [UIView animateWithDuration:0.5 animations:^{
        currentResourceImgView.frame = lastFrame;
    } completion:^(BOOL finished){
        currentResourceImgView.hidden = true;
        currentResourceImgView.contentMode = UIViewContentModeCenter;
        [vapp resumeAR:nil];
        [self sendSubviewToBack:currentResourceImgView];
    }];
}
-(void)playVideo{
    zoomIn = false;
    selectedView.hidden = true;
    [playBtn removeFromSuperview];
    playBtn = nil;
    currentResourceImgView.hidden = true;
    currentResourceImgView.contentMode = UIViewContentModeCenter;
    [vapp resumeAR:nil];
    [self sendSubviewToBack:currentResourceImgView];
    [self.delegate imageTargetOpenVideo:self.actualTrackable.actualSpot.videoPath];
}
#pragma mark - Auxiliar
-(void)changeActualTrackable:(Trackable *)actTrackable{
    self.actualTrackable = actTrackable;
    if(actTrackable.actualSpot == nil){
        currentImage = [UIImage imageNamed:@"AvailableSpot"];
        [self loadTextureWithImage:currentImage];
        didLoadCustomImage = false;
    }else{
        NSString *imagesDirectoryPath = [ToolBox getImagesDirectory];
        NSString *documentPath = [imagesDirectoryPath stringByAppendingPathComponent:self.actualTrackable.actualSpot.spotPath];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:documentPath]];
        if(image){
            currentImage = image;
            [self loadTextureWithImage:currentImage];
        }else{
            currentImage = [UIImage imageNamed:@"AvailableSpot"];
            [self loadTextureWithImage:currentImage];
            didLoadCustomImage = false;
        }
    }
}
-(UIImage *)getCurrentVideoImage{
    lastFrame = currentResourceImgView.frame;
    int width = ((int)self.frame.size.width) * 2;
    int height = ((int)self.frame.size.height) * 2;
    NSInteger myDataLength = width * height * 4;
    GLubyte *buffer = (GLubyte *) malloc(myDataLength);
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    GLubyte *buffer2 = (GLubyte *) malloc(myDataLength);
    for(int y = 0; y < height; y++){
        for(int x = 0; x < width * 4; x++){
            buffer2[(height - y - 1) * width * 4 + x] = buffer[y * 4 * width + x];
        }
    }
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, buffer2, myDataLength, NULL);
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    int bytesPerRow = 4 * width;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent, bitsPerPixel, bytesPerRow,     colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
    return [UIImage imageWithCGImage:imageRef];
}
@end
