//
//  Extensions.swift
//  Tu Point
//
//  Created by Victor Salazar on 11/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import UIKit
extension UIColor {
    convenience init(r:Int, g:Int, b:Int, a:CGFloat = 1){
        let red = CGFloat(r) / 256.0
        let green = CGFloat(g) / 256.0
        let blue = CGFloat(b) / 256.0
        self.init(red: red, green: green, blue: blue, alpha: a)
    }
    convenience init(rgb:Int, a:CGFloat = 1){
        self.init(r: rgb, g: rgb, b: rgb, a: a)
    }
}
