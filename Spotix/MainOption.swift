//
//  MainOption.swift
//  Spotix
//
//  Created by Victor Salazar on 13/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import Foundation
import UIKit
class MainOption{
    var backHexColor:String
    var image:String
    var title:String
    var resource:ImageTargetResource!
    init(nBackHexColor:String, nImage:String, nTitle:String, nResource:ImageTargetResource = .note){
        backHexColor = nBackHexColor
        image = nImage
        title = nTitle
        resource = nResource
    }
}
