//
//  FriendsViewController.swift
//  Spotix
//
//  Created by victor salazar on 11/21/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class FriendsViewController:UIViewController, UITableViewDataSource, UITableViewDelegate {
    let arrAllContacts = ["Erick Arrasco G.", "Daniloo Boy Vela", "Junior Nevado", "Brunella de Vela", "Kristel Barrios", "Ivette Boy", "Katty Talledo de Nevado"]
    var spot:Spot!
    var arrSelectedContacts:Array<Int> = []
    //MARK: - IBOutlet
    @IBOutlet weak var backBtn:UIButton!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        backBtn.setImage(UIImage(named: "Back")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
        if let selectedFriend = spot.selectedFriends {
            arrSelectedContacts = selectedFriend.components(separatedBy: ",").map(){($0 as NSString).integerValue}
        }
    }
    //MARK: - IBAction
    @IBAction func back(){
        dismiss(animated: true, completion: nil)
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrAllContacts.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        cell.contactNameLbl.text = arrAllContacts[indexPath.row]
        cell.selectedBtn.layer.borderColor = UIColor(rgb: 132).cgColor
        if arrSelectedContacts.contains(indexPath.row) {
            cell.selectedBtn.setImage(UIImage(named: "Check"), for: .normal)
            cell.selectedBtn.backgroundColor = UIColor(rgb: 132)
        }else{
            cell.selectedBtn.setImage(nil, for: .normal)
            cell.selectedBtn.backgroundColor = UIColor.clear
        }
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        if arrSelectedContacts.contains(indexPath.row) {
            arrSelectedContacts.remove(at: arrSelectedContacts.index(of: indexPath.row)!)
        }else{
            arrSelectedContacts.append(indexPath.row)
        }
        tableView.reloadData()
        let selectedFriends = arrSelectedContacts.map(){"\($0)"}.joined(separator: ",")
        Spot.addFriends(selectedFriends, to: spot)
    }
}
