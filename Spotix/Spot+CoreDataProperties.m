//
//  Spot+CoreDataProperties.m
//  Spotix
//
//  Created by victor salazar on 11/11/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import "Spot+CoreDataProperties.h"
@implementation Spot(CoreDataProperties)
+(NSFetchRequest<Spot *> *)fetchRequest{
	return [[NSFetchRequest alloc] initWithEntityName:@"Spot"];
}
@dynamic imagePath;
@dynamic spotPath;
@dynamic type;
@dynamic videoPath;
@dynamic userId;
@dynamic isPublic;
@dynamic trackable;
@dynamic creationDate;
@dynamic selectedFriends;
@end
