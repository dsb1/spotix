//
//  Trackable+CoreDataClass.h
//  Spotix
//
//  Created by Victor Salazar on 11/9/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@class Spot;
NS_ASSUME_NONNULL_BEGIN
@interface Trackable:NSManagedObject
+(NSArray *)getTrackables;
+(Trackable *)createTrackable:(NSDictionary *)dicTrackable;
@end
NS_ASSUME_NONNULL_END
#import "Trackable+CoreDataProperties.h"
