//
//  ImageTargetViewController.m
//  Spotix
//
//  Created by Victor Salazar on 14/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import "ImageTargetViewController.h"
#import <Vuforia/TrackerManager.h>
#import <Vuforia/ObjectTracker.h>
#import <Vuforia/Trackable.h>
#import <Vuforia/DataSet.h>
#import <Photos/Photos.h>
#import <GoogleMaps/GoogleMaps.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <Spotix-Swift.h>
#import "AlbumPhotoCollectionViewCell.h"
#import "ImageTargetsEAGLView.h"
#import "SampleApplicationSession.h"
#import "Trackable+CoreDataClass.h"
#import "Spot+CoreDataClass.h"
@interface ImageTargetViewController()<SampleApplicationControl, ImageTargetDelegate, UITextViewDelegate>{
    ImageTargetsEAGLView *eaglView;
    SampleApplicationSession *vapp;
    Vuforia::DataSet *dataSetSpotixDemo;
    Vuforia::DataSet *dataSetCurrent;
    PHCachingImageManager *imageManager;
    PHFetchResult *arrPhotos;
    UIButton *addBtn;
    UICollectionView *albumPhotosCollectionView;
    UITextView *noteTxtView;
    UIView *albumPhotosView, *noteMainView, *imageOptionsView, *editPhotoView;
    int indexSelectedPhoto;
    Trackable *actualTrackable;
    UIImage *videoImg;
}
@end
@implementation ImageTargetViewController
-(void)loadView{
    indexSelectedPhoto = -1;
    actualTrackable = [[Trackable getTrackables] firstObject];
    vapp = [[SampleApplicationSession alloc] initWithDelegate:self];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGRect viewFrame = screenBounds;
    if(YES == vapp.isRetinaDisplay){
        viewFrame.size.width *= [UIScreen mainScreen].nativeScale;
        viewFrame.size.height *= [UIScreen mainScreen].nativeScale;
    }
    eaglView = [[ImageTargetsEAGLView alloc] initWithFrame:viewFrame appSession:vapp];
    eaglView.delegate = self;   
    [self setView:eaglView];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.glResource = eaglView;
    
    [vapp initAR:Vuforia::GL_20 orientation:self.preferredInterfaceOrientationForPresentation];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(self.imageTargetResource != ImageTargetResourceNote){
        [self processPhotoLibraryAuthorizationStatus:[PHPhotoLibrary authorizationStatus]];
    }
    if(vapp.cameraIsStarted){
        [vapp resumeAR:nil];
        if(actualTrackable.actualSpot){
            [eaglView changeActualTrackable:actualTrackable];
        }
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(addBtn){
        addBtn.hidden = true;
    }
    [eaglView clearFrameBuffer];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.destinationViewController isKindOfClass:[ImageElementsViewController class]]){
        ImageElementsViewController *viewCont = segue.destinationViewController;
        viewCont.loadingImages = self.imageTargetResource == ImageTargetResourceImage;
        viewCont.actualTrackable = eaglView.actualTrackable;
        viewCont.videoImage = videoImg;
    }else if([segue.destinationViewController isKindOfClass:[CapturePhotoVideoViewController class]]){
        CapturePhotoVideoViewController *viewCont = segue.destinationViewController;
        viewCont.isCapturingPhoto = (self.imageTargetResource == ImageTargetResourceImage);
        viewCont.actualTrackable = eaglView.actualTrackable;
        viewCont.videoImage = videoImg;
    }else if([[segue destinationViewController] isKindOfClass:[RestrictSpotViewController class]]){
        RestrictSpotViewController *viewCont = segue.destinationViewController;
        viewCont.trackable = eaglView.actualTrackable;
    }
}
#pragma mark - IBAction
-(IBAction)close:(id)sender{
    [vapp stopAR:nil];
    [self finishOpenGLESCommands];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.glResource = nil;
    [self dismissViewControllerAnimated:true completion:nil];
}
-(void)addResource:(id)sender{
    [vapp pauseAR:nil];
    if(self.imageTargetResource == ImageTargetResourceNote){
        if(noteMainView == nil){
            noteMainView = [[UIView alloc] initWithFrame:self.view.frame];
            noteMainView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75];
            UIView *noteInsideView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height - 40)];
            noteInsideView.backgroundColor = [UIColor whiteColor];
            [noteMainView addSubview:noteInsideView];
            UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [closeBtn setFrame:CGRectMake(20, 20, 40, 40)];
            [closeBtn setTitle:@"X" forState:UIControlStateNormal];
            [closeBtn setBackgroundColor:[UIColor redColor]];
            closeBtn.layer.cornerRadius = 20;
            [closeBtn addTarget:self action:@selector(dismissNoteView) forControlEvents:UIControlEventTouchUpInside];
            [noteMainView addSubview:closeBtn];
            noteTxtView = [[UITextView alloc] initWithFrame:CGRectMake((noteMainView.frame.size.width - 256) / 2, 40, 256, 256)];
            noteTxtView.textAlignment = NSTextAlignmentCenter;
            noteTxtView.font = [UIFont systemFontOfSize:20];
            noteTxtView.layer.borderWidth = 1;
            noteTxtView.delegate = self;
            noteTxtView.text = @"A";
            [noteInsideView addSubview:noteTxtView];
            noteTxtView.text = @"";
            UIButton *acceptBtn = [[UIButton alloc] initWithFrame:CGRectMake((noteMainView.frame.size.width - 50) / 2, CGRectGetMaxY(noteTxtView.frame) + 20, 50, 50)];
            [acceptBtn setImage:[UIImage imageNamed:@"AcceptPhoto"] forState:UIControlStateNormal];
            [acceptBtn addTarget:self action:@selector(acceptText) forControlEvents:UIControlEventTouchUpInside];
            [noteInsideView addSubview:acceptBtn];
            [self.view addSubview:noteMainView];
        }else{
            noteMainView.hidden = false;
        }
        [noteTxtView becomeFirstResponder];
    }else{
        if(!albumPhotosView){
            albumPhotosView = [[UIView alloc] initWithFrame:self.view.frame];
            albumPhotosView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.75];
            UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
            flowLayout.itemSize = CGSizeMake(200, 200);
            flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            flowLayout.minimumInteritemSpacing = 10;
            flowLayout.minimumLineSpacing = 10;
            albumPhotosCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 200, self.view.frame.size.width, 200) collectionViewLayout:flowLayout];
            [albumPhotosCollectionView registerClass:[AlbumPhotoCollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
            albumPhotosCollectionView.backgroundColor = [UIColor whiteColor];
            [albumPhotosCollectionView setDelegate:self];
            [albumPhotosCollectionView setDataSource:self];
            [albumPhotosView addSubview:albumPhotosCollectionView];
            UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [closeBtn setFrame:CGRectMake(20, self.view.frame.size.height - 220, 40, 40)];
            [closeBtn setTitle:@"X" forState:UIControlStateNormal];
            [closeBtn setBackgroundColor:[UIColor redColor]];
            closeBtn.layer.cornerRadius = 20;
            [closeBtn addTarget:self action:@selector(dismissAlbumPhotos) forControlEvents:UIControlEventTouchUpInside];
            [albumPhotosView addSubview:closeBtn];
            UIButton *galleryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [galleryBtn setFrame:CGRectMake(20, self.view.frame.size.height - 60, 40, 40)];
            [galleryBtn setImage:[UIImage imageNamed:@"Gallery"] forState:UIControlStateNormal];
            [galleryBtn addTarget:self action:@selector(showGallery) forControlEvents:UIControlEventTouchUpInside];
            [albumPhotosView addSubview:galleryBtn];
            [self.view addSubview:albumPhotosView];
        }else{
            albumPhotosView.hidden = false;
        }
    }
}
-(void)dismissAlbumPhotos{
    albumPhotosView.hidden = true;
    [vapp resumeAR:nil];
}
-(void)dismissNoteView{
    [noteTxtView resignFirstResponder];
    noteMainView.hidden = true;
    [vapp resumeAR:nil];
}
-(void)showGallery{
    albumPhotosView.hidden = true;
    [self performSegueWithIdentifier:@"showImageElements" sender:nil];
}
-(void)editPhoto{
    [self acceptPhoto];
}
-(void)acceptPhoto{
    AlbumPhotoCollectionViewCell *cell = (AlbumPhotoCollectionViewCell *)[albumPhotosCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:indexSelectedPhoto inSection:0]];
    UIImage *finalImage = [ToolBox convertSquaredImage:cell.photoImgView.image];
    int type = (self.imageTargetResource == ImageTargetResourceVideo) ? 1 : 2;
    if(type == 1){
        PHAsset *asset = [arrPhotos objectAtIndex:indexSelectedPhoto];
        [imageManager requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
            AVURLAsset *urlAsset = (AVURLAsset *)asset;
            [Spot creatSpotWithImage:finalImage withVideoImage:videoImg withType:type withVideoPath:urlAsset.URL.path withTrackable:eaglView.actualTrackable isPublic:true withUserId:1];
        }];
        [eaglView loadTextureWithImage:finalImage];
        [self dismissAlbumPhotos];
    }else{
        [Spot creatSpotWithImage:finalImage withVideoImage:videoImg withType:type withVideoPath:nil withTrackable:eaglView.actualTrackable isPublic:true withUserId:1];
        
        [eaglView loadTextureWithImage:finalImage];
        [self dismissAlbumPhotos];
    }
}
-(void)acceptText{
    [noteTxtView resignFirstResponder];
    UIGraphicsBeginImageContext(CGSizeMake(256, 256));
    [[UIColor whiteColor] set];
    UIRectFill(CGRectMake(0, 0, 256, 256));
    NSString *note = noteTxtView.text;
    CGFloat y = MAX(0, 256 - noteTxtView.contentSize.height);
    CGRect noteFrame = CGRectMake(0, y / 2, 256, 256 - y);
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [note drawInRect:noteFrame withAttributes:@{NSFontAttributeName: noteTxtView.font, NSParagraphStyleAttributeName: paragraphStyle}];
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [Spot creatSpotWithImage:finalImage withVideoImage:videoImg withType:0 withVideoPath:nil withTrackable:eaglView.actualTrackable isPublic:true withUserId:1];
    [eaglView loadTextureWithImage:finalImage];
    [self dismissNoteView];
}
-(void)showOptionsSelectedImage{
    [vapp pauseAR:nil];
    videoImg = [eaglView getCurrentVideoImage];
    if(imageOptionsView == nil){
        imageOptionsView = [[UIView alloc] initWithFrame:self.view.frame];
        imageOptionsView.backgroundColor = [[UIColor alloc] initWithRgb:54 a:1];
        UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        closeBtn.frame = CGRectMake(self.view.frame.size.width - 40, 30, 16, 16);
        [closeBtn setTitle:@"X" forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(closeOptionsImage) forControlEvents:UIControlEventTouchUpInside];
        [closeBtn setTintColor:[UIColor whiteColor]];
        
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, 20)];
        titleLbl.textColor = [UIColor whiteColor];
        titleLbl.textAlignment = NSTextAlignmentCenter;
        titleLbl.text = @"Image Options";
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(30, 64, self.view.frame.size.width - 60, 1)];
        lineView.backgroundColor = [[UIColor alloc] initWithRgb:136 a:1];
        
        CGFloat spaceBetweenOptions = (self.view.frame.size.height - 65 - 120 - 60) / 5;
        NSArray *arrOptionsTitles = nil;
        if(eaglView.actualTrackable.actualSpot.userId.intValue == 1){
            if(self.imageTargetResource == ImageTargetResourceNote){
                arrOptionsTitles =  @[@"Share", @"Restrict", @"Change Note"];
            }else if(self.imageTargetResource == ImageTargetResourceImage) {
                arrOptionsTitles =  @[@"Share", @"Take photo", @"Restrict", @"Change Image"];
            }else{
                arrOptionsTitles =  @[@"Share", @"Take video", @"Restrict", @"Change Video"];
            }
        }else{
            arrOptionsTitles =  @[@"Share"];
        }
        for(int i = 0; i < arrOptionsTitles.count; i++){
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(0, 65 + 30 + spaceBetweenOptions * (i + 1) + 30 * i, self.view.frame.size.width, 30);
            [btn setTitle:arrOptionsTitles[i] forState:UIControlStateNormal];
            btn.tintColor = [UIColor whiteColor];
            btn.tag = i;
            [btn addTarget:self action:@selector(optionSelectedImage:) forControlEvents:UIControlEventTouchUpInside];
            [imageOptionsView addSubview:btn];
        }
        [imageOptionsView addSubview:lineView];
        [imageOptionsView addSubview:titleLbl];
        [imageOptionsView addSubview:closeBtn];
        [self.view addSubview:imageOptionsView];
    }else{
        imageOptionsView.hidden = false;
        imageOptionsView = nil;
    }
}
-(void)closeOptionsImage{
    imageOptionsView.hidden = true;
    imageOptionsView = nil;
    [vapp resumeAR:nil];
}
-(void)optionSelectedImage:(UIButton *)btn{
    if(btn.tag == 0){
        imageOptionsView.hidden = true;
        imageOptionsView = nil;
        NSString *lastImageURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"com.dsb.spotix.currentImage"];
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true) firstObject];
        NSURL *documentsDirectoryURL = [NSURL fileURLWithPath:documentsPath];
        NSURL *documentURL = [documentsDirectoryURL URLByAppendingPathComponent:lastImageURL];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:documentURL]];
        NSArray *arrActivityItems = @[image];
        UIActivityViewController *activityViewCont = [[UIActivityViewController alloc] initWithActivityItems:arrActivityItems applicationActivities:nil];
        [activityViewCont setCompletionWithItemsHandler:^(UIActivityType activityType, BOOL completed, NSArray *returnedItems, NSError *activityError){
            [vapp resumeAR:nil];
        }];
        [self presentViewController:activityViewCont animated:true completion:nil];
    }else{
        imageOptionsView.hidden = true;
        imageOptionsView = nil;
        if(self.imageTargetResource == ImageTargetResourceNote){
            if(btn.tag == 1){
                //Restringir
                [self performSegueWithIdentifier:@"showRestrictSpot" sender:nil];
            }else if(btn.tag == 2){
                [self addResource:nil];
            }
        }else{
            if(btn.tag == 1){
                //Tomar foto/video
                if(self.imageTargetResource == ImageTargetResourceImage){
                    //[self performSegueWithIdentifier:@"showCapturePhotoVideo" sender:nil];
                }
                [self performSegueWithIdentifier:@"showCapturePhotoVideo" sender:nil];
            }else if(btn.tag == 2){
                //Restringir
                [self performSegueWithIdentifier:@"showRestrictSpot" sender:nil];
            }else if(btn.tag == 3){
                [self addResource:nil];
            }
        }
    }
}
#pragma mark - SampleApplicationControl
-(bool)doInitTrackers{
    Vuforia::TrackerManager &trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::Tracker *trackerBase = trackerManager.initTracker(Vuforia::ObjectTracker::getClassType());
    return trackerBase != NULL;
}
-(bool)doDeinitTrackers{
    Vuforia::TrackerManager &trackerManager = Vuforia::TrackerManager::getInstance();
    trackerManager.deinitTracker(Vuforia::ObjectTracker::getClassType());
    return true;
}
-(bool)doLoadTrackersData{
    dataSetSpotixDemo = [self loadObjectTrackerDataSet:[NSString stringWithFormat:@"%@.xml", actualTrackable.fileName]];
    if(dataSetSpotixDemo == NULL){
        return false;
    }
    if(![self activateDataset:dataSetSpotixDemo]){
        return false;
    }
    return true;
}
-(bool)doUnloadTrackersData{
    [self deactivateDataSet:dataSetCurrent];
    dataSetCurrent = NULL;
    Vuforia::TrackerManager &trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker *objectTracker = static_cast<Vuforia::ObjectTracker *>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    objectTracker->destroyDataSet(dataSetSpotixDemo);
    return true;
}
-(void)onInitARDone:(NSError *)error{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(self.view.frame.size.width / 2 - 20, self.view.frame.size.height - 80, 40, 40)];
    [btn setTitle:@"X" forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor redColor]];
    btn.layer.cornerRadius = 20;
    [btn addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    if(error == NULL){
        NSError *vufError = NULL;
        [vapp startAR:Vuforia::CameraDevice::CAMERA_DIRECTION_BACK error:&vufError];
        Vuforia::CameraDevice::getInstance().setFocusMode(Vuforia::CameraDevice::FOCUS_MODE_CONTINUOUSAUTO);
    }else{
        NSLog(@"error on onInitARDone");
    }
}
-(bool)doStopTrackers{
    Vuforia::TrackerManager &trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::Tracker* tracker = trackerManager.getTracker(Vuforia::ObjectTracker::getClassType());
    if(tracker != NULL){
        tracker->stop();
        return true;
    }
    return false;
}
-(bool)doStartTrackers{
    Vuforia::TrackerManager &trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::Tracker* tracker = trackerManager.getTracker(Vuforia::ObjectTracker::getClassType());
    if(tracker == 0){
        return false;
    }
    tracker->start();
    return true;
}
#pragma mark - Auxiliar
-(Vuforia::DataSet *)loadObjectTrackerDataSet:(NSString *)dataFile{
    Vuforia::DataSet *dataSet = NULL;
    Vuforia::TrackerManager &trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker *objectTracker = static_cast<Vuforia::ObjectTracker *>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    if(objectTracker != NULL){
        dataSet = objectTracker->createDataSet();
        if(dataSet != NULL){
            if(!dataSet->load([dataFile cStringUsingEncoding:NSASCIIStringEncoding], Vuforia::STORAGE_APPRESOURCE)){
                objectTracker->destroyDataSet(dataSet);
                dataSet = NULL;
            }
        }
    }
    return dataSet;
}
-(BOOL)activateDataset:(Vuforia::DataSet *)dataSet{
    if(dataSetCurrent != NULL){
        [self deactivateDataSet:dataSetCurrent];
    }
    BOOL success = false;
    Vuforia::TrackerManager &trackerManager = Vuforia::TrackerManager::getInstance();
    Vuforia::ObjectTracker *objectTracker = static_cast<Vuforia::ObjectTracker *>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
    if(objectTracker != NULL){
        if(objectTracker->activateDataSet(dataSet)){
            dataSetCurrent = dataSet;
            success = true;
        }
    }
    if(success){
        [self setExtendedTrackingForDataSet:dataSet start:true];
    }
    return success;
}
-(BOOL)deactivateDataSet:(Vuforia::DataSet *)dataSet{
    BOOL success = false;
    if(dataSetCurrent != NULL && dataSetCurrent == dataSet) {
        [self setExtendedTrackingForDataSet:dataSet start:false];
        Vuforia::TrackerManager &trackerManager = Vuforia::TrackerManager::getInstance();
        Vuforia::ObjectTracker *objectTracker = static_cast<Vuforia::ObjectTracker *>(trackerManager.getTracker(Vuforia::ObjectTracker::getClassType()));
        if(objectTracker != NULL){
            if(objectTracker->deactivateDataSet(dataSet)){
                success = true;
            }
        }
    }
    dataSetCurrent = NULL;
    return success;
}
-(BOOL)setExtendedTrackingForDataSet:(Vuforia::DataSet *)dataSet start:(BOOL)start{
    BOOL result = true;
    for(int i = 0; i < dataSet->getNumTrackables(); i++){
        Vuforia::Trackable *trackable = dataSet->getTrackable(i);
        if(start){
            if(!trackable->startExtendedTracking()){
                NSLog(@"Failed to start extended tracking on: %s", trackable->getName());
                result = false;
            }
        }else{
            if(!trackable->stopExtendedTracking()){
                NSLog(@"Failed to stop extended tracking on: %s", trackable->getName());
                result = false;
            }
        }
    }
    return true;
}
-(void)finishOpenGLESCommands{
    [eaglView finishOpenGLESCommands];
    [eaglView freeOpenGLESResources];
}
-(void)processPhotoLibraryAuthorizationStatus:(PHAuthorizationStatus)status{
    if(status == PHAuthorizationStatusNotDetermined){
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
            [self processPhotoLibraryAuthorizationStatus:status];
        }];
    }else if(status == PHAuthorizationStatusAuthorized){
        imageManager = [[PHCachingImageManager alloc] init];
        PHFetchOptions *fetchOptions = [PHFetchOptions new];
        [fetchOptions setPredicate:[NSPredicate predicateWithFormat:@"mediaType == %d", self.imageTargetResource == ImageTargetResourceVideo ? PHAssetMediaTypeVideo : PHAssetMediaTypeImage]];
        [fetchOptions setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:false]]];
        arrPhotos = [PHAsset fetchAssetsWithOptions:fetchOptions];
    }else{
        [ToolBox showAlertWithTitle:@"Alert" withMessage:@"You dont have access to photo library. Go to Settings and change it." withOkButtonTitle:@"OK" withOkHandler:^(UIAlertAction * _Nonnull alertAction) {
            [self dismissViewControllerAnimated:true completion:nil];
        } inViewCont:self];
    }
}
-(void)adjustTextContentSize{
    CGFloat deadSpace = noteTxtView.bounds.size.height - noteTxtView.contentSize.height;
    CGFloat offsetY = MAX(0, deadSpace / 2);
    noteTxtView.contentInset = UIEdgeInsetsMake(offsetY, 0, noteTxtView.contentInset.bottom, 0);
}
-(Trackable *)getActualTrackable{
    return actualTrackable;
}
#pragma mark - ImageTarget
-(void)imageTargetShowingMask:(bool)showingMask{
    if(showingMask){
        if(!addBtn){
            addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [addBtn setFrame:CGRectMake(self.view.frame.size.width - 80, self.view.frame.size.height - 90, 60, 60)];
            addBtn.layer.cornerRadius = 30;
            UIImage *image = [[UIImage imageNamed:@"MenuUser"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [addBtn setTintColor:[UIColor whiteColor]];
            [addBtn setImage:image forState:UIControlStateNormal];
            addBtn.backgroundColor = [[UIColor alloc] initWithRgb:54 a:1];
            [addBtn addTarget:self action:@selector(showOptionsSelectedImage) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:addBtn];
        }else{
            addBtn.hidden = false;
        }
    }else{
        if(addBtn){
            addBtn.hidden = true;
        }
    }
}
-(void)imageTargetShouldLoadResource{
    videoImg = [eaglView getCurrentVideoImage];
    [self addResource:nil];
}
-(void)imageTargetOpenVideo:(NSString *)videoPath{
    NSURL *url = [NSURL fileURLWithPath:videoPath];
    AVPlayerViewController *playerViewCont = [AVPlayerViewController new];
    playerViewCont.player = [AVPlayer playerWithURL:url];
    [self presentViewController:playerViewCont animated:true completion:^{
        playerViewCont.player.play;
    }];
}
#pragma mark - CollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrPhotos.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    AlbumPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    PHAsset *asset = [arrPhotos objectAtIndex:indexPath.item];
    [imageManager requestImageForAsset:asset targetSize:CGSizeMake(800, 800) contentMode:PHImageContentModeAspectFit options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info){
        cell.photoImgView.image = result;
    }];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    indexSelectedPhoto = (int)indexPath.item;
    [self acceptPhoto];
}
#pragma mark - TextView
-(void)textViewDidChange:(UITextView *)textView{
    [self adjustTextContentSize];
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self adjustTextContentSize];
    return true;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return true;
}
@end
