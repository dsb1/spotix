//
//  MainOptionsViewController.swift
//  Spotix
//
//  Created by Victor Salazar on 13/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class MainOptionsViewController:UIViewController, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout {
    //MARK: - Constants
    let arrMainOptions = [MainOption(nBackHexColor: "303031", nImage: "Note", nTitle: "Note", nResource: .note), MainOption(nBackHexColor: "dc4c4c", nImage: "Video", nTitle: "Video", nResource: .video), MainOption(nBackHexColor: "424a60", nImage: "Image", nTitle: "Image", nResource: .image)]
    var spaceX:CGFloat = 0
    //MARK: - IBOutlet
    @IBOutlet weak var mainOptionsCollectionView:UICollectionView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        mainOptionsCollectionView.decelerationRate = UIScrollViewDecelerationRateFast
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont  = segue.destination as? ImageTargetViewController {
            let selectedOption = (mainOptionsCollectionView.indexPathsForSelectedItems!.first! as NSIndexPath).item
            viewCont.imageTargetResource = arrMainOptions[selectedOption].resource
        }
    }
    //MARK: - IBAction
    @IBAction func showMenu(){
        if let viewCont = navigationController?.parent as? UserMainViewController {
            viewCont.showMenu()
        }
    }
    //MARK: - CollectionView
    func collectionView(_ collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        return arrMainOptions.count
    }
    func collectionView(_ collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath:IndexPath) -> CGSize {
        let cellWidth = view.frame.width - 120
        spaceX = cellWidth / 2 + 15
        return CGSize(width: cellWidth, height: cellWidth * 11 / 10)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "optionCell", for: indexPath) as! MainOptionCollectionViewCell
        let mainOption = arrMainOptions[indexPath.item]
        cell.backView.backgroundColor = ToolBox.convertHexColorToColor(mainOption.backHexColor)
        cell.optionImgView.image = UIImage(named: "\(mainOption.image)Option")
        cell.optionDescriptionLbl.text = "Place \(mainOption.title)"
        return cell
    }
    //MARK: - ScrollView
    func scrollViewDidEndDecelerating(_ scrollView:UIScrollView){
        let offsetX = scrollView.contentOffset.x
        var newContentOffset = CGPoint.zero
        if offsetX > spaceX {
            if offsetX < spaceX * 3{
                newContentOffset = CGPoint(x: spaceX * 2, y: 0)
            }else{
                newContentOffset = CGPoint(x: spaceX * 4, y: 0)
            }
        }
        scrollView.setContentOffset(newContentOffset, animated: true)
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate:Bool){
        scrollViewDidEndDecelerating(scrollView)
    }
}
