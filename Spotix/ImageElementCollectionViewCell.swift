//
//  ElementCollectionViewCell.swift
//  Spotix
//
//  Created by victor salazar on 10/26/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class ImageElementCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var elementImgView:UIImageView!
    @IBOutlet weak var playImgView:UIImageView!
}
