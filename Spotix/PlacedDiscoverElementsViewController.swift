//
//  PlacedElementsViewController.swift
//  Spotix
//
//  Created by victor salazar on 11/8/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class PlacedDiscoverElementsViewController:UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK: - ViewCont
    var userMainViewCont:UserMainViewController!
    let arrOptions = ["All", "Notes", "Videos", "Images"]
    var currenOption = 0 {
        didSet{
            showTxtFld.text = arrOptions[currenOption]
        }
    }
    var arrAllSpots:Array<Spot> = []
    var arrCurrentSpots:Array<Spot> = []
    var loadMySpots:Bool = true
    //MARK: - IBOutlet
    @IBOutlet weak var backBtn:UIButton!
    @IBOutlet weak var showTxtFld:UITextField!
    @IBOutlet weak var spotCollectionView:UICollectionView!
    @IBOutlet weak var titleLbl:UILabel!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        titleLbl.text = loadMySpots ? "Placed Elements" : "Discover Elements"
        backBtn.setImage(UIImage(named: "Back")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
        let picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self
        showTxtFld.inputView = picker
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let dondeBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(acceptOption))
        toolBar.items = [flexibleSpace, dondeBtn]
        toolBar.sizeToFit()
        showTxtFld.inputAccessoryView = toolBar
        reloadSpots()
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        if loadMySpots {
            arrAllSpots = Spot.getMySpots() as! Array<Spot>
        }else{
            arrAllSpots = Spot.getOthersSpots() as! Array<Spot>
        }
        reloadSpots()
    }
    override func viewDidAppear(_ animated:Bool){
        super.viewDidAppear(animated)
        userMainViewCont = self.parent as! UserMainViewController
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? DetailElementViewController {
            viewCont.spot = arrCurrentSpots[spotCollectionView.indexPathsForSelectedItems!.first!.item]
        }
    }
    //MARK: - IBAction
    @IBAction func back(){
        userMainViewCont.selectOption(option: -1)
    }
    func acceptOption(){
        showTxtFld.resignFirstResponder()
        reloadSpots()
    }
    //MARK: - PickerView
    func numberOfComponents(in pickerView:UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView:UIPickerView, numberOfRowsInComponent component:Int) -> Int {
        return arrOptions.count
    }
    func pickerView(_ pickerView:UIPickerView, titleForRow row:Int, forComponent component:Int) -> String? {
        return arrOptions[row]
    }
    func pickerView(_ pickerView:UIPickerView, didSelectRow row:Int, inComponent component:Int) {
        currenOption = row
    }
    //MARK: - CollectionView
    func collectionView(_ collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath:IndexPath) -> CGSize {
        let cellWidth = (view.frame.width - 52) / 2
        return CGSize(width: cellWidth, height: cellWidth * 1.25)
    }
    func collectionView(_ collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        return arrCurrentSpots.count
    }
    func collectionView(_ collectionView:UICollectionView, cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "placedElementCell", for: indexPath) as! ElementCollectionViewCell
        let spot = arrCurrentSpots[indexPath.item]
        let imagesDirectoryPath = ToolBox.getImagesDirectory() as NSString
        let documentPath = imagesDirectoryPath.appendingPathComponent(spot.spotPath!)
        cell.elementImgView.image = UIImage(contentsOfFile: documentPath)
        cell.elementTypeLbl.text = ["Note", "Video", "Image"][spot.type!.intValue]
        cell.locationNameLbl.text = spot.trackable?.locationName
        return cell
    }
    func reloadSpots(){
        if currenOption == 0 {
            arrCurrentSpots = arrAllSpots
        }else{
            arrCurrentSpots = arrAllSpots.filter(){$0.type!.intValue == (currenOption - 1)}
        }
        spotCollectionView.reloadData()
    }
}
