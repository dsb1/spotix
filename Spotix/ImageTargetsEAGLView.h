/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/
#import <UIKit/UIKit.h>
#import <Vuforia/UIGLViewProtocol.h>
#import "Texture.h"
#import "SampleApplicationSession.h"
#import "SampleGLResourceHandler.h"
#import "SampleAppRenderer.h"

@protocol ImageTargetDelegate
-(void)imageTargetShowingMask:(bool)showingMask;
-(void)imageTargetShouldLoadResource;
-(void)imageTargetOpenVideo:(NSString *)videoPath;
@end

@class Trackable;
@interface ImageTargetsEAGLView:UIView<UIGLViewProtocol, SampleGLResourceHandler, SampleAppRendererControl>{
    EAGLContext *context;
    GLuint defaultFramebuffer, colorRenderbuffer, depthRenderbuffer, shaderProgramID;
    Texture *actualTexture;
    SampleAppRenderer *sampleAppRenderer;
    UIView *selectedView;
    UIImageView *currentResourceImgView;
    UIImage *currentImage;
    BOOL zoomIn, didLoadCustomImage;
    CGRect lastFrame;
    NSArray *arrTrackables;
    UIButton *playBtn;
}
@property(nonatomic, strong) Trackable *actualTrackable;
@property(nonatomic, weak) SampleApplicationSession *vapp;
@property(nonatomic, weak) id<ImageTargetDelegate> delegate;
-(id)initWithFrame:(CGRect)frame appSession:(SampleApplicationSession *)app;
-(void)finishOpenGLESCommands;
-(void)freeOpenGLESResources;
-(void)loadTextureWithImage:(UIImage *)image;
-(void)clearFrameBuffer;
-(void)changeActualTrackable:(Trackable *)actTrackable;
-(UIImage *)getCurrentVideoImage;
@end
