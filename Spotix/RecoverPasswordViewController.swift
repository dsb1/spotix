//
//  RecoverPasswordViewController.swift
//  Spotix
//
//  Created by victor salazar on 8/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class RecoverPasswordViewController:UIViewController{
    //MARK: - IBOutlet
    @IBOutlet weak var userNameTxtFld:UITextField!
    @IBOutlet weak var emailTxtFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
    }
    //MARK: - IBAction
    @IBAction func back(){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissKeyboard(){
        self.userNameTxtFld.resignFirstResponder()
        self.emailTxtFld.resignFirstResponder()
    }
}
