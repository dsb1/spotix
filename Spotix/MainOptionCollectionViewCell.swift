//
//  MainOptionCollectionViewCell.swift
//  Spotix
//
//  Created by Victor Salazar on 13/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class MainOptionCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var optionImgView:UIImageView!
    @IBOutlet weak var optionDescriptionLbl:UILabel!
}