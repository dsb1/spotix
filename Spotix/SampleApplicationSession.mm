/*===============================================================================
Copyright (c) 2015-2016 PTC Inc. All Rights Reserved.

 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/
#import "SampleApplicationSession.h"
#import "SampleApplicationUtils.h"
#import <Vuforia/Vuforia.h>
#import <Vuforia/Vuforia_iOS.h>
#import <Vuforia/Tool.h>
#import <Vuforia/Renderer.h>
#import <Vuforia/CameraDevice.h>
#import <Vuforia/VideoBackgroundConfig.h>
#import <Vuforia/UpdateCallback.h>
#import <UIKit/UIKit.h>

#define DEBUG_SAMPLE_APP 1

namespace{
    SampleApplicationSession *instance = nil;
    int mVuforiaInitFlags;
    Vuforia::CameraDevice::CAMERA_DIRECTION mCamera = Vuforia::CameraDevice::CAMERA_DIRECTION_DEFAULT;
    class VuforiaApplication_UpdateCallback:public Vuforia::UpdateCallback{
        virtual void Vuforia_onUpdate(Vuforia::State& state);
    } vuforiaUpdate;

    NSString * SAMPLE_APPLICATION_ERROR_DOMAIN = @"vuforia_sample_application";
}

@implementation SampleApplicationSession
@synthesize viewport;
-(id)initWithDelegate:(id<SampleApplicationControl>)delegate{
    if(self = [super init]){
        self.delegate = delegate;
        instance = self;
    }
    return self;
}
-(NSError *)NSErrorWithCode:(int)code{
    return [NSError errorWithDomain:SAMPLE_APPLICATION_ERROR_DOMAIN code:code userInfo:nil];
}
-(NSError *)NSErrorWithCode:(NSString *)description code:(NSInteger)code{
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: description};
    return [NSError errorWithDomain:SAMPLE_APPLICATION_ERROR_DOMAIN code:code userInfo:userInfo];
}
-(NSError *)NSErrorWithCode:(int)code error:(NSError **)error{
    if(error != NULL){
        *error = [self NSErrorWithCode:code];
        return *error;
    }
    return nil;
}
-(BOOL)isRetinaDisplay{
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && 1.0 < [UIScreen mainScreen].scale);
}
-(void) initAR:(int) VuforiaInitFlags orientation:(UIInterfaceOrientation) ARViewOrientation {
    self.cameraIsActive = NO;
    self.cameraIsStarted = NO;
    mVuforiaInitFlags = VuforiaInitFlags;
    self.isRetinaDisplay = [self isRetinaDisplay];
    self.mARViewOrientation = ARViewOrientation;
    [self performSelectorInBackground:@selector(initVuforiaInBackground) withObject:nil];
}
-(void)initVuforiaInBackground{
    @autoreleasepool {
        Vuforia::setInitParameters(mVuforiaInitFlags,"AQsW7ib/////AAAAGWNs9LCpd012sy9Pe/yavAkv+OAfimBzD9rGv0uHJhxWhb2r4z+vx4EMKETH0qVj/umRmQN3B52B9GAEd4ypvFsFSKHB3v6Scp4P+rSWA4UU0VjT7LsoQ5T09sER1F9hkVuc5E3TLZ4d8CgoxGGeK2HOKKgkJ2hsYiTTmqvEfDulDXLhDkSGFi2HMen1x0zXGrVfpNHvPBDia2vrmeBUeaX7heuPSX/7VYP5qxZxWjgF0J0ToA3GQEnOZWzQ6iqIo7c66iUBx7JiiBr1SdcHPCaVLJ2mURWxRGeWiBKT9z4iU+eCDWJtqeEH8Yt7HNMVJzU5FEAVRM1qgZ7UsDwECxp8QJ2xiv30mrkgC0xn0SKu");
        
        NSInteger initSuccess = 0;
        do{
            initSuccess = Vuforia::init();
        }while(0 <= initSuccess && 100 > initSuccess);
        if(initSuccess == 100){
            [self performSelectorOnMainThread:@selector(prepareAR) withObject:nil waitUntilDone:NO];
        }else{
            if(Vuforia::INIT_NO_CAMERA_ACCESS == initSuccess){
                [self performSelectorOnMainThread:@selector(showCameraAccessWarning) withObject:nil waitUntilDone:YES];
            }else{
                NSError *error;
                switch(initSuccess){
                    case Vuforia::INIT_LICENSE_ERROR_NO_NETWORK_TRANSIENT:
                        error = [self NSErrorWithCode:NSLocalizedString(@"INIT_LICENSE_ERROR_NO_NETWORK_TRANSIENT", nil) code:initSuccess];
                        break;
                    case Vuforia::INIT_LICENSE_ERROR_NO_NETWORK_PERMANENT:
                        error = [self NSErrorWithCode:NSLocalizedString(@"INIT_LICENSE_ERROR_NO_NETWORK_PERMANENT", nil) code:initSuccess];
                        break;
                    case Vuforia::INIT_LICENSE_ERROR_INVALID_KEY:
                        error = [self NSErrorWithCode:NSLocalizedString(@"INIT_LICENSE_ERROR_INVALID_KEY", nil) code:initSuccess];
                        break;
                    case Vuforia::INIT_LICENSE_ERROR_CANCELED_KEY:
                        error = [self NSErrorWithCode:NSLocalizedString(@"INIT_LICENSE_ERROR_CANCELED_KEY", nil) code:initSuccess];
                        break;
                    case Vuforia::INIT_LICENSE_ERROR_MISSING_KEY:
                        error = [self NSErrorWithCode:NSLocalizedString(@"INIT_LICENSE_ERROR_MISSING_KEY", nil) code:initSuccess];
                        break;
                    case Vuforia::INIT_LICENSE_ERROR_PRODUCT_TYPE_MISMATCH:
                        error = [self NSErrorWithCode:NSLocalizedString(@"INIT_LICENSE_ERROR_PRODUCT_TYPE_MISMATCH", nil) code:initSuccess];
                        break;
                    default:
                        error = [self NSErrorWithCode:NSLocalizedString(@"INIT_default", nil) code:initSuccess];
                        break;
                }
                [self.delegate onInitARDone:error];
            }
        }
    }
}
-(void)showCameraAccessWarning{
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString *message = [NSString stringWithFormat:@"User denied camera access to this App. To restore camera access, go to: \nSettings > Privacy > Camera > %@ and turn it ON.", appName];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iOS8 Camera Access Warning" message:message delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if([alertView.title isEqualToString:@"iOS8 Camera Access Warning"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kDismissAppViewController" object:nil];
    }
}
-(bool)resumeAR:(NSError **)error{
    Vuforia::onResume();
    if((self.cameraIsStarted) && (!self.cameraIsActive)){
        if(!Vuforia::CameraDevice::getInstance().init(mCamera)){
            [self NSErrorWithCode:E_INITIALIZING_CAMERA error:error];
            return NO;
        }
        if(!Vuforia::CameraDevice::getInstance().start()) {
            [self NSErrorWithCode:E_STARTING_CAMERA error:error];
            return NO;
        }
        self.cameraIsActive = YES;
    }
    return YES;
}
-(bool)pauseAR:(NSError **)error{
    if(self.cameraIsActive){
        if(! Vuforia::CameraDevice::getInstance().stop()) {
            [self NSErrorWithCode:E_STOPPING_CAMERA error:error];
            return NO;
        }
        if(! Vuforia::CameraDevice::getInstance().deinit()) {
            [self NSErrorWithCode:E_DEINIT_CAMERA error:error];
            return NO;
        }
        self.cameraIsActive = NO;
    }
    Vuforia::onPause();
    return YES;
}
-(void)Vuforia_onUpdate:(Vuforia::State *)state{
    if((self.delegate != nil) && [self.delegate respondsToSelector:@selector(onVuforiaUpdate:)]){
        [self.delegate onVuforiaUpdate:state];
    }
}
-(CGSize)getCurrentARViewBoundsSize{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGSize viewSize = screenBounds.size;
    if(self.isRetinaDisplay) {
        viewSize.width *= [UIScreen mainScreen].nativeScale;
        viewSize.height *= [UIScreen mainScreen].nativeScale;
    }
    return viewSize;
}
-(void)prepareAR{
    Vuforia::registerCallback(&vuforiaUpdate);
    Vuforia::onSurfaceCreated();
    CGSize viewBoundsSize = [self getCurrentARViewBoundsSize];
    int smallerSize = MIN(viewBoundsSize.width, viewBoundsSize.height);
    int largerSize = MAX(viewBoundsSize.width, viewBoundsSize.height);
    if(self.mARViewOrientation == UIInterfaceOrientationPortrait){
        Vuforia::onSurfaceChanged(smallerSize, largerSize);
        Vuforia::setRotation(Vuforia::ROTATE_IOS_90);
        self.mIsActivityInPortraitMode = YES;
    }else if(self.mARViewOrientation == UIInterfaceOrientationPortraitUpsideDown){
        Vuforia::onSurfaceChanged(smallerSize, largerSize);
        Vuforia::setRotation(Vuforia::ROTATE_IOS_270);
        self.mIsActivityInPortraitMode = YES;
    }else if(self.mARViewOrientation == UIInterfaceOrientationLandscapeLeft){
        Vuforia::onSurfaceChanged(largerSize, smallerSize);
        Vuforia::setRotation(Vuforia::ROTATE_IOS_180);
        self.mIsActivityInPortraitMode = NO;
    }else if (self.mARViewOrientation == UIInterfaceOrientationLandscapeRight){
        Vuforia::onSurfaceChanged(largerSize, smallerSize);
        Vuforia::setRotation(Vuforia::ROTATE_IOS_0);
        self.mIsActivityInPortraitMode = NO;
    }
    [self initTracker];
}
-(void)initTracker{
    if(![self.delegate doInitTrackers]){
        [self.delegate onInitARDone:[self NSErrorWithCode:E_INIT_TRACKERS]];
        return;
    }
    [self loadTrackerData];
}
-(void)loadTrackerData{
    [self performSelectorInBackground:@selector(loadTrackerDataInBackground) withObject:nil];
}
-(void)loadTrackerDataInBackground{
    @autoreleasepool{
        if(! [self.delegate doLoadTrackersData]) {
            [self.delegate onInitARDone:[self NSErrorWithCode:E_LOADING_TRACKERS_DATA]];
            return;
        }
    }
    [self.delegate onInitARDone:nil];
}
-(void)configureVideoBackgroundWithViewWidth:(float)viewWidth andHeight:(float)viewHeight{
    Vuforia::CameraDevice& cameraDevice = Vuforia::CameraDevice::getInstance();
    Vuforia::VideoMode videoMode = cameraDevice.getVideoMode(Vuforia::CameraDevice::MODE_DEFAULT);
    Vuforia::VideoBackgroundConfig config;
    config.mEnabled = true;
    config.mPosition.data[0] = 0.0f;
    config.mPosition.data[1] = 0.0f;
    if(self.mIsActivityInPortraitMode){
        float aspectRatioVideo = (float)videoMode.mWidth / (float)videoMode.mHeight;
        float aspectRatioView = viewHeight / viewWidth;
        if(aspectRatioVideo < aspectRatioView){
            config.mSize.data[0] = (int)videoMode.mHeight * (viewHeight / (float)videoMode.mWidth);
            config.mSize.data[1] = (int)viewHeight;
        }else{
            config.mSize.data[0] = (int)viewWidth;
            config.mSize.data[1] = (int)videoMode.mWidth * (viewWidth / (float)videoMode.mHeight);
        }
    }else{
        if(viewWidth < viewHeight){
            float temp = viewWidth;
            viewWidth = viewHeight;
            viewHeight = temp;
        }
        float aspectRatioVideo = (float)videoMode.mWidth / (float)videoMode.mHeight;
        float aspectRatioView = viewWidth / viewHeight;
        if(aspectRatioVideo < aspectRatioView){
            config.mSize.data[0] = (int)viewWidth;
            config.mSize.data[1] = (int)videoMode.mHeight * (viewWidth / (float)videoMode.mWidth);
        }else{
            config.mSize.data[0] = (int)videoMode.mWidth * (viewHeight / (float)videoMode.mHeight);
            config.mSize.data[1] = (int)viewHeight;
        }
    }
    viewport.posX = ((viewWidth - config.mSize.data[0]) / 2) + config.mPosition.data[0];
    viewport.posY = (((int)(viewHeight - config.mSize.data[1])) / (int) 2) + config.mPosition.data[1];
    viewport.sizeX = config.mSize.data[0];
    viewport.sizeY = config.mSize.data[1];
    
#ifdef DEBUG_SAMPLE_APP
    NSLog(@"VideoBackgroundConfig: size: %d,%d", config.mSize.data[0], config.mSize.data[1]);
    NSLog(@"VideoMode:w=%d h=%d", videoMode.mWidth, videoMode.mHeight);
    NSLog(@"width=%7.3f height=%7.3f", viewWidth, viewHeight);
    NSLog(@"ViewPort: X,Y: %d,%d Size X,Y:%d,%d", viewport.posX,viewport.posY,viewport.sizeX,viewport.sizeY);
#endif
    
    Vuforia::Renderer::getInstance().setVideoBackgroundConfig(config);
}
-(bool)startCamera:(Vuforia::CameraDevice::CAMERA_DIRECTION)camera viewWidth:(float)viewWidth andHeight:(float)viewHeight error:(NSError **)error{
    if(! Vuforia::CameraDevice::getInstance().init(camera)){
        [self NSErrorWithCode:-1 error:error];
        return NO;
    }
    if(!Vuforia::CameraDevice::getInstance().selectVideoMode(Vuforia::CameraDevice::MODE_DEFAULT)){
        [self NSErrorWithCode:-1 error:error];
        return NO;
    }
    [self configureVideoBackgroundWithViewWidth:viewWidth andHeight:viewHeight];
    int recommendedFps = Vuforia::Renderer::getInstance().getRecommendedFps();
    Vuforia::Renderer::getInstance().setTargetFps(recommendedFps);
    if(!Vuforia::CameraDevice::getInstance().start()){
        [self NSErrorWithCode:-1 error:error];
        return NO;
    }
    mCamera = camera;
    if(![self.delegate doStartTrackers]){
        [self NSErrorWithCode:-1 error:error];
        return NO;
    }
    const Vuforia::CameraCalibration& cameraCalibration = Vuforia::CameraDevice::getInstance().getCameraCalibration();
    _projectionMatrix = Vuforia::Tool::getProjectionGL(cameraCalibration, 2.0f, 5000.0f);
    return YES;
}
-(bool)startAR:(Vuforia::CameraDevice::CAMERA_DIRECTION)camera error:(NSError **)error{
    CGSize ARViewBoundsSize = [self getCurrentARViewBoundsSize];
    if(![self startCamera:camera viewWidth:ARViewBoundsSize.width andHeight:ARViewBoundsSize.height error:error]){
        return NO;
    }
    self.cameraIsActive = YES;
    self.cameraIsStarted = YES;
    return YES;
}
-(bool)stopAR:(NSError **)error{
    if(self.cameraIsActive){
        Vuforia::CameraDevice::getInstance().stop();
        Vuforia::CameraDevice::getInstance().deinit();
        self.cameraIsActive = NO;
    }
    self.cameraIsStarted = NO;
    if(![self.delegate doStopTrackers]){
        [self NSErrorWithCode:E_STOPPING_TRACKERS error:error];
        return NO;
    }
    if(![self.delegate doUnloadTrackersData]){
        [self NSErrorWithCode:E_UNLOADING_TRACKERS_DATA error:error];
        return NO;
    }
    if(![self.delegate doDeinitTrackers]){
        [self NSErrorWithCode:E_DEINIT_TRACKERS error:error];
        return NO;
    }
    Vuforia::onPause();
    Vuforia::deinit();
    return YES;
}
-(bool)stopCamera:(NSError **)error{
    if(self.cameraIsActive){
        Vuforia::CameraDevice::getInstance().stop();
        Vuforia::CameraDevice::getInstance().deinit();
        self.cameraIsActive = NO;
    }else{
        [self NSErrorWithCode:E_CAMERA_NOT_STARTED error:error];
        return NO;
    }
    self.cameraIsStarted = NO;
    if(![self.delegate doStopTrackers]){
        [self NSErrorWithCode:E_STOPPING_TRACKERS error:error];
        return NO;
    }
    return YES;
}
-(void)errorMessage:(NSString *)message{
    [[[UIAlertView alloc] initWithTitle:SAMPLE_APPLICATION_ERROR_DOMAIN message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}
void VuforiaApplication_UpdateCallback::Vuforia_onUpdate(Vuforia::State &state){
    if(instance != nil){
        [instance Vuforia_onUpdate:&state];
    }
}
@end
