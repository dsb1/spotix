//
//  Spot+CoreDataClass.m
//  Spotix
//
//  Created by Victor Salazar on 11/9/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import "Spot+CoreDataClass.h"
#import <Photos/Photos.h>
#import <Spotix-Swift.h>
#import "Trackable+CoreDataClass.h"
@implementation Spot
+(void)creatSpotWithImage:(UIImage *)spotImage withVideoImage:(UIImage *)videoImage withType:(int)type withVideoPath:(NSString * _Nullable)videoPath withTrackable:(Trackable *)trackable isPublic:(BOOL)isPublic withUserId:(int)userId{
    if(trackable.actualSpot){
        [self deleteSpot:trackable.actualSpot];
    }
    NSString *imagesDirectoryPath = [ToolBox getImagesDirectory];
    NSString *fileName = [[trackable.locationName componentsSeparatedByString:@" "] componentsJoinedByString:@""];
    NSString *spotPath = [imagesDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@Spot.png", fileName]];
    NSString *imagePath = [imagesDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", fileName]];
    if([[NSFileManager defaultManager] fileExistsAtPath:spotPath]){
        [[NSFileManager defaultManager] removeItemAtPath:spotPath error:nil];
    }
    if([[NSFileManager defaultManager] fileExistsAtPath:imagePath]){
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
    }
    [UIImagePNGRepresentation(spotImage) writeToFile:spotPath atomically:true];
    [UIImagePNGRepresentation(videoImage) writeToFile:imagePath atomically:true];
    NSManagedObjectContext *context = [AppDelegate getMOC];
    Spot *newSpot = [NSEntityDescription insertNewObjectForEntityForName:@"Spot" inManagedObjectContext:context];
    newSpot.type = [NSNumber numberWithInt:type];
    newSpot.spotPath = spotPath.lastPathComponent;
    newSpot.imagePath = imagePath.lastPathComponent;
    newSpot.videoPath = videoPath;
    newSpot.trackable = trackable;
    newSpot.isPublic = [NSNumber numberWithBool:isPublic];
    newSpot.userId = [NSNumber numberWithInt:userId];
    newSpot.creationDate = [NSDate date];
    NSError *error;
    [context save:&error];
    NSLog(@"%s error: %@", __func__, error);
}
+(void)deleteSpot:(Spot *)spot{
    NSManagedObjectContext *context = [AppDelegate getMOC];
    [context deleteObject:spot];
    NSError *error;
    [context save:&error];
    NSLog(@"%s error: %@", __func__, error);
}
+(NSArray *)getMySpots{
    NSManagedObjectContext *context = [AppDelegate getMOC];
    NSFetchRequest<Spot *> *fecthRequest = [Spot fetchRequest];
    [fecthRequest setPredicate:[NSPredicate predicateWithFormat:@"userId = 1"]];
    NSArray *arrResults = [context executeFetchRequest:fecthRequest error:nil];
    if(arrResults == nil){
        return @[];
    }
    return arrResults;
}
+(NSArray *)getOthersSpots{
    NSManagedObjectContext *context = [AppDelegate getMOC];
    NSFetchRequest<Spot *> *fecthRequest = [Spot fetchRequest];
    [fecthRequest setPredicate:[NSPredicate predicateWithFormat:@"userId != 1"]];
    NSArray *arrResults = [context executeFetchRequest:fecthRequest error:nil];
    if(arrResults == nil){
        return @[];
    }
    return arrResults;
}
+(void)makeSpot:(Spot *)spot public:(BOOL)isPublic{
    NSManagedObjectContext *context = [AppDelegate getMOC];
    spot.isPublic = [NSNumber numberWithBool:isPublic];
    spot.selectedFriends = @"";
    NSError *error;
    [context save:&error];
    NSLog(@"%s error: %@", __func__, error);
}
+(void)addFriends:(NSString *)selectedFriends toSpot:(Spot *)spot{
    NSManagedObjectContext *context = [AppDelegate getMOC];
    spot.selectedFriends = selectedFriends;
    NSError *error;
    [context save:&error];
    NSLog(@"%s error: %@", __func__, error);
    
}
@end
