//
//  DetailElementViewController.swift
//  Spotix
//
//  Created by victor salazar on 11/11/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
import AVKit
import AVFoundation
import GoogleMaps
class DetailElementViewController:UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    //MARK: - Variables
    var spot:Spot!
    var arrOptions:Array<MainOption> = []
    //MARK: - IBOutlet
    @IBOutlet weak var backSpotImgView:UIImageView!
    @IBOutlet weak var spotImgView:UIImageView!
    @IBOutlet weak var backBtn:UIButton!
    @IBOutlet weak var optionsCollectionView:UICollectionView!
    @IBOutlet weak var optionsCollectionViewWidthCons:NSLayoutConstraint!
    @IBOutlet weak var infoView:UIView!
    @IBOutlet weak var creationDateLbl:UILabel!
    @IBOutlet weak var locationLbl:UILabel!
    @IBOutlet weak var restrictLbl:UILabel!
    @IBOutlet weak var placedByLbl:UILabel!
    @IBOutlet weak var rateBackView:UIView!
    @IBOutlet weak var rateView:RateView!
    @IBOutlet weak var playBtn:UIButton!
    @IBOutlet weak var mapView:GMSMapView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        if spot.isPublic!.boolValue || spot.userId!.intValue == 1 {
            arrOptions.append(MainOption(nBackHexColor: "", nImage: "Share", nTitle: "share"))
            arrOptions.append(MainOption(nBackHexColor: "", nImage: "Comment", nTitle: "comment"))
            arrOptions.append(MainOption(nBackHexColor: "", nImage: "Star", nTitle: "star"))
        }
        arrOptions.append(MainOption(nBackHexColor: "", nImage: "Info", nTitle: "info"))
        if spot.userId!.intValue == 1 {
            arrOptions.append(MainOption(nBackHexColor: "", nImage: "Trash", nTitle: "trash"))
        }
        optionsCollectionViewWidthCons.constant = CGFloat(arrOptions.count * 33 + (arrOptions.count - 1) * 15)
        backBtn.setImage(UIImage(named: "Back")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
        backSpotImgView.image = ToolBox.getImageWith(path: spot.imagePath!)
        spotImgView.image = ToolBox.getImageWith(path: spot.spotPath!)
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMMM dd, yyyy, h:mm a"
        creationDateLbl.text = dateFormat.string(from: spot.creationDate!)
        locationLbl.text = spot.trackable?.locationName
        restrictLbl.text = spot.isPublic!.boolValue ? "Public" : "Friends"
        placedByLbl.text = ["Erick1988", "Victor415", "Zico2980"][spot.userId!.intValue - 1]
        if spot.type!.intValue != 1 {
            playBtn.isHidden = true
        }
        let spotCoordinate = CLLocationCoordinate2D(latitude: spot.trackable!.latitude!.doubleValue, longitude: spot.trackable!.longitude!.doubleValue)
        let camera = GMSCameraPosition.camera(withTarget: spotCoordinate, zoom: 16)
        mapView.camera = camera
        let marker = GMSMarker(position: spotCoordinate)
        marker.map = mapView
    }
    //MARK: - IBAction
    @IBAction func back(){
        dismiss(animated: true, completion: nil)
    }
    @IBAction func hideInfo(){
        infoView.isHidden = true
    }
    @IBAction func rate(){
        rateBackView.isHidden = true
    }
    @IBAction func hideRate(){
        rateBackView.isHidden = true
    }
    @IBAction func rating(_ tapGesture:UITapGestureRecognizer){
        let point = tapGesture.location(in: rateView)
        let starWidth = rateView.frame.width / 5.0
        let actualRate:Int = Int(floor(point.x / starWidth)) + 1
        rateView.rate = CGFloat(actualRate)
    }
    @IBAction func playVideo(){
        let url = URL(fileURLWithPath: spot.videoPath!)
        let playerViewCont = AVPlayerViewController()
        playerViewCont.player = AVPlayer(url: url)
        present(playerViewCont, animated: true, completion: {
            playerViewCont.player?.play()
        })
    }
    //MARK: - CollectionView
    func collectionView(_ collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        return arrOptions.count
    }
    func collectionView(_ collectionView:UICollectionView, cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "optionCell", for: indexPath) as! DetailElementOptionCollectionViewCell
        let image = arrOptions[indexPath.item].image
        cell.optionImgView.image = UIImage(named: image)
        cell.optionImgView.layer.borderColor = UIColor.white.cgColor
        return cell
    }
    func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath:IndexPath){
        let option = arrOptions[indexPath.item].title
        if option == "share" {
            let arrActivityItems = [spotImgView.image!]
            let activityViewCont = UIActivityViewController(activityItems: arrActivityItems, applicationActivities: nil)
            present(activityViewCont, animated: true, completion: nil)
        }else if option == "comment" {
            performSegue(withIdentifier: "showComments", sender: nil)
        }else if option == "star" {
            rateBackView.isHidden = false
        }else if option == "info" {
            infoView.isHidden = false
        }else if option == "trash" {
            let alertCont = UIAlertController(title: "Alert", message: "Are you sure want to delete this spot?", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action:UIAlertAction) in
                Spot.delete(self.spot)
                self.dismiss(animated: true, completion: nil)
            }))
            present(alertCont, animated: true, completion: nil)
        }
    }
}
