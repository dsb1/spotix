//
//  LoginViewController.swift
//  Spotix
//
//  Created by victor salazar on 7/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class LoginViewController:UIViewController{
    //MARK: - Variables
    var didViewLayoutSubview = false
    //MARK: - IBOutlet
    @IBOutlet weak var spotixIconImgView:UIImageView!
    @IBOutlet weak var loginView:UIView!
    @IBOutlet weak var userNameTxtFld:UITextField!
    @IBOutlet weak var passwordTxtFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        if !self.didViewLayoutSubview {
            self.didViewLayoutSubview = true
            self.spotixIconImgView.transform = CGAffineTransform(translationX: 0, y: (self.view.frame.height - self.spotixIconImgView.frame.height) / 2.0 - self.spotixIconImgView.frame.origin.y)
        }
    }
    override func viewDidAppear(_ animated:Bool){
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 1.5, delay: 0.1, options: UIViewAnimationOptions(), animations: {
            self.spotixIconImgView.transform = CGAffineTransform.identity
            self.loginView.alpha = 1
            }, completion: nil)
    }
    //MARK: - IBAction
    @IBAction func dismissKeyboard(){
        self.userNameTxtFld.resignFirstResponder()
        self.passwordTxtFld.resignFirstResponder()
    }
    @IBAction func login(){
        self.performSegue(withIdentifier: "showMain", sender: nil)
        /*let req = NSMutableURLRequest(URL: NSURL(string: "https://desvconecta.pacificovida.net/OrganizateGet/Get.svc/GetData/btafur/9996/6D7C73/samsung/SM-T810/Android.6.0.1/020000000000/R52G9160M3W")!, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 30)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(req, completionHandler: {data, res, error -> Void in
            if error == nil {
                let dicResult = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                print("\(dicResult)")
            }else{
                print(error)
            }
        })
        task.resume()*/
    }
}
