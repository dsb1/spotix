//
//  ContactTableViewCell.swift
//  Spotix
//
//  Created by victor salazar on 11/21/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class ContactTableViewCell:UITableViewCell{
    @IBOutlet weak var selectedBtn:UIButton!
    @IBOutlet weak var contactNameLbl:UILabel!
    @IBOutlet weak var bottomView:UIView!
}
