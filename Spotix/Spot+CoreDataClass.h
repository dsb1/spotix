//
//  Spot+CoreDataClass.h
//  Spotix
//
//  Created by Victor Salazar on 11/9/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
@class Trackable;
NS_ASSUME_NONNULL_BEGIN
@interface Spot:NSManagedObject
+(NSArray *)getMySpots;
+(NSArray *)getOthersSpots;
+(void)deleteSpot:(Spot *)spot;
+(void)creatSpotWithImage:(UIImage *)spotImage withVideoImage:(UIImage *)videoImage withType:(int)type withVideoPath:(NSString * _Nullable)videoPath withTrackable:(Trackable *)trackable isPublic:(BOOL)isPublic withUserId:(int)userId;
+(void)makeSpot:(Spot *)spot public:(BOOL)isPublic;
+(void)addFriends:(NSString *)selectedFriends toSpot:(Spot *)spot;
@end
NS_ASSUME_NONNULL_END
#import "Spot+CoreDataProperties.h"
