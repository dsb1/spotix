//
//  AlbumPhotoCollectionViewCell.h
//  Spotix
//
//  Created by victor salazar on 10/4/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import <UIKit/UIKit.h>
@interface AlbumPhotoCollectionViewCell:UICollectionViewCell{
    UIView *selectionView;
}
@property(nonatomic, strong) UIImageView *photoImgView;
@property(nonatomic, strong) UIButton *editBtn;
@property(nonatomic, strong) UIButton *acceptBtn;
-(void)updateVisibilitySelectionView:(BOOL)show;
@end
