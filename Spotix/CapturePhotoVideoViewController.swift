//
//  CapturePhotoVideoViewController.swift
//  Spotix
//
//  Created by victor salazar on 10/27/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
import AVFoundation
import PhotosUI
class CapturePhotoVideoViewController:UIViewController, AVCaptureFileOutputRecordingDelegate {
    //MARK: - Variables
    let cameraSession = AVCaptureSession()
    var isCapturingPhoto:Bool = false
    var currentDevicePosition:AVCaptureDevicePosition = .back
    var procesingData = false
    var actualTrackable:Trackable!
    var videoImage:UIImage!
    var previewLayer:AVCaptureVideoPreviewLayer!
    //MARK: - IBOutlet
    @IBOutlet weak var closeBtn:UIButton!
    @IBOutlet weak var flashBtn:UIButton!
    @IBOutlet weak var cameraView:UIView!
    @IBOutlet weak var topView:UIView!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var loadingView:UIView!
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    //MARK: - View Cont
    override func viewDidLoad(){
        super.viewDidLoad()
        closeBtn.setImage(UIImage(named: "Close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        if isCapturingPhoto {
            cameraSession.sessionPreset = AVCaptureSessionPresetPhoto
        }else{
            cameraSession.sessionPreset = AVCaptureSessionPresetHigh
        }
        loadDevice()
    }
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        previewLayer = AVCaptureVideoPreviewLayer(session: self.cameraSession)!
        if isCapturingPhoto {
            previewLayer.bounds = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.width)
            previewLayer.position = CGPoint(x: previewLayer.bounds.width / 2, y: previewLayer.bounds.height / 2)
        }else{
            previewLayer.bounds = self.view.bounds
            previewLayer.position = CGPoint(x: previewLayer.bounds.width / 2, y: previewLayer.bounds.height / 2 - cameraView.frame.origin.y)
        }
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        cameraView.layer.insertSublayer(previewLayer, at: 0)
        cameraSession.startRunning()
    }
    //MARK: - IBAction
    @IBAction func close(){
        dismiss(animated: true, completion: nil)
    }
    @IBAction func switchCamera(){
        if currentDevicePosition == .back {
            currentDevicePosition = .front
        }else{
            currentDevicePosition = .back
        }
        loadDevice()
    }
    @IBAction func changeFlashMode(){
        if let currentCameraInput = cameraSession.inputs.first as? AVCaptureDeviceInput {
            if let device = currentCameraInput.device {
                if device.flashMode == .off {
                    try! device.lockForConfiguration()
                    device.flashMode = .on
                    if device.isTorchModeSupported(.on) {
                        device.torchMode = .on
                    }
                    device.unlockForConfiguration()
                }else{
                    try! device.lockForConfiguration()
                    device.flashMode = .off
                    if device.isTorchModeSupported(.off) {
                        device.torchMode = .off
                    }
                    device.unlockForConfiguration()
                }
            }
        }
    }
    @IBAction func capturePhotoVideo(){
        if isCapturingPhoto {
            if !procesingData {
                procesingData = true
                if let dataOutput = cameraSession.outputs.first as? AVCaptureStillImageOutput {
                    if let videoConnection = dataOutput.connection(withMediaType: AVMediaTypeVideo) {
                        loadingView.isHidden = false
                        activityIndicator.startAnimating()
                        dataOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: {(sampleBuffer:CMSampleBuffer?, error:Error?) in
                            self.cameraSession.stopRunning()
                            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                            let image = UIImage(data: imageData!)!
                            UIGraphicsBeginImageContext(CGSize(width: image.size.width, height: image.size.width))
                            image.draw(in: CGRect(x: 0 , y: -64, width: image.size.width, height: image.size.height))
                            let finalImage = UIGraphicsGetImageFromCurrentImageContext()!
                            UIGraphicsEndImageContext()
                            Spot.creatSpot(with: finalImage, withVideoImage: self.videoImage, withType: Int32(2), withVideoPath: nil, with: self.actualTrackable, isPublic: true, withUserId: 1)
                            let presentingViewCont = self.presentingViewController
                            self.dismiss(animated: true, completion: {
                                if let viewCont = presentingViewCont as? ImageElementsViewController {
                                    viewCont.back()
                                }
                            })
                        })
                    }
                }
            }
        }else{
            stopVideo()
        }
    }
    @IBAction func startVideo(){
        if !isCapturingPhoto && !procesingData {
            if let dataOutput = cameraSession.outputs.first as? AVCaptureMovieFileOutput {
                let path = "\(NSTemporaryDirectory())movie.mov"
                if FileManager.default.fileExists(atPath: path) {
                    try! FileManager.default.removeItem(atPath: path)
                }
                let movieUrl = URL(fileURLWithPath: path)
                dataOutput.startRecording(toOutputFileURL: movieUrl, recordingDelegate: self)
            }
        }
    }
    @IBAction func stopVideo(){
        procesingData = true
        loadingView.isHidden = false
        activityIndicator.startAnimating()
        cameraSession.stopRunning()
        if let dataOutput = cameraSession.outputs.first as? AVCaptureMovieFileOutput {
            dataOutput.stopRecording()
        }
    }
    //MARK: - Movie
    func capture(_ captureOutput:AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL:URL!, fromConnections connections:[Any]!, error:Error!){
        if error == nil {
            UISaveVideoAtPathToSavedPhotosAlbum(outputFileURL.path, self, #selector(video(_:didFinishSavingWithError:contextInfo:)), nil)
        }else{
            ToolBox.showAlertWithTitle("Alert", withMessage: "There was an error saving your video. Try again", inViewCont: self)
        }
    }
    //MARK: - Auxiliar
    func loadDevice(){
        cameraSession.beginConfiguration()
        if let currentCameraInput = cameraSession.inputs.first as? AVCaptureInput {
            cameraSession.removeInput(currentCameraInput)
        }
        var captureDevice:AVCaptureDevice?
        for capDevice in AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) {
            if let device = capDevice as? AVCaptureDevice {
                if device.position == currentDevicePosition {
                    captureDevice = device
                    break
                }
            }
        }
        if captureDevice == nil {
            captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) as AVCaptureDevice
        }
        var newVideoInput:AVCaptureDeviceInput?
        do{
            newVideoInput = try AVCaptureDeviceInput(device: captureDevice)
        }catch{}
        if newVideoInput != nil {
            if cameraSession.canAddInput(newVideoInput) {
                cameraSession.addInput(newVideoInput)
            }
        }
        if cameraSession.outputs.count == 0 {
            if isCapturingPhoto {
                let dataOutput = AVCaptureStillImageOutput()
                dataOutput.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                if(cameraSession.canAddOutput(dataOutput) == true){
                    cameraSession.addOutput(dataOutput)
                }
                /*if #available(iOS 10.0, *) {
                    let dataOutput = AVCapturePhotoOutput()
                    if(cameraSession.canAddOutput(dataOutput) == true){
                        cameraSession.addOutput(dataOutput)
                    }
                }else{
                    let dataOutput = AVCaptureStillImageOutput()
                    dataOutput.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                    if(cameraSession.canAddOutput(dataOutput) == true){
                        cameraSession.addOutput(dataOutput)
                    }
                }*/
            }else{
                let dataOutput = AVCaptureMovieFileOutput()
                if(cameraSession.canAddOutput(dataOutput) == true){
                    cameraSession.addOutput(dataOutput)
                }
            }
        }
        cameraSession.commitConfiguration()
        flashBtn.isEnabled = currentDevicePosition == .back
        flashBtn.setImage(UIImage(named: (currentDevicePosition == .back ? "" : "")), for: .normal)
        if currentDevicePosition == .back {
            flashBtn.setImage(UIImage(named: "Flash"), for: .normal)
        }else{
            flashBtn.setImage(UIImage(named: "FlashDisable"), for: .normal)
        }
    }
    func video(_ videoPath:String, didFinishSavingWithError error:Error, contextInfo context:Any){
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.video.rawValue)
        let arrVideos = PHAsset.fetchAssets(with: fetchOptions)
        let lastVideo = arrVideos.firstObject!
        let imageManager = PHCachingImageManager()
        imageManager.requestAVAsset(forVideo: lastVideo, options: nil, resultHandler:{(asset:AVAsset?, audioMix:AVAudioMix?, info:[AnyHashable:Any]?) in
            let urlAsset = asset as! AVURLAsset
            
            let imageGenerator = AVAssetImageGenerator(asset: urlAsset)
            imageGenerator.appliesPreferredTrackTransform = true
            var time = urlAsset.duration
            time.value = 0
            let imageRef = try! imageGenerator.copyCGImage(at: time, actualTime: nil)
            let finalImage = UIImage(cgImage: imageRef)
            let squaredImage = ToolBox.convertSquaredImage(finalImage)
            Spot.creatSpot(with: squaredImage, withVideoImage: self.videoImage, withType: 1, withVideoPath: urlAsset.url.path, with: self.actualTrackable, isPublic: true, withUserId: 1)
            let presentingViewCont = self.presentingViewController
            self.dismiss(animated: true, completion: {
                if let viewCont = presentingViewCont as? ImageElementsViewController {
                    viewCont.back()
                }
            })
        })
    }
}
