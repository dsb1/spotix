//
//  AppDelegate.swift
//  Spotix
//
//  Created by Victor Salazar on 31/08/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
import CoreData
import GoogleMaps
@UIApplicationMain class AppDelegate:UIResponder, UIApplicationDelegate {
    //MARK: - Variables
    var window: UIWindow?
    var glResource:SampleGLResourceHandler?
    //MARK: - ApplicationDelegate
    func application(_ application:UIApplication, didFinishLaunchingWithOptions launchOptions:[UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyB-wAD0ihFXhMGnBvKCeUJG9aGz1gU3Y_Y")
        let imagesDirec = ToolBox.getImagesDirectory()
        if !FileManager.default.fileExists(atPath: imagesDirec, isDirectory: nil) {
            try! FileManager.default.createDirectory(atPath: imagesDirec, withIntermediateDirectories: false, attributes: nil)
        }
        let imagesDirecURL = URL(fileURLWithPath: imagesDirec, isDirectory: true)
        var isExcludedFromBackup:AnyObject?
        try! (imagesDirecURL as NSURL).getResourceValue(&isExcludedFromBackup, forKey: URLResourceKey.isExcludedFromBackupKey)
        if let numIsExcludedFromBackup = isExcludedFromBackup as? NSNumber {
            if !numIsExcludedFromBackup.boolValue {
                try! (imagesDirecURL as NSURL).setResourceValue(true, forKey: URLResourceKey.isExcludedFromBackupKey)
            }
        }
        let trackables = Trackable.getTrackables()
        if trackables.count == 0 {
            let dicTrackableOftalmoSalud:Dictionary<String, Any> = ["fileName": "SpotixDemo", "latitude": NSNumber(value: -12.0902357), "longitude": NSNumber(value: -77.0174212), "locationName": "Oftalmo Salud"]
            let oftalmoTrackable = Trackable.createTrackable(dicTrackableOftalmoSalud)
            let oftalmoSaludBack = UIImage(named: "oftalmosaludBack.jpg")
            let oftalmoSaludSpot = UIImage(named: "oftalmosaludSpot.png")
            Spot.creatSpot(with: oftalmoSaludSpot!, withVideoImage: oftalmoSaludBack!, withType: 2, withVideoPath: nil, with: oftalmoTrackable, isPublic: true, withUserId: 2)
            
            let dicTrackableClinicaRicardo:Dictionary<String, Any> = ["fileName": "SpotixDemo", "latitude": NSNumber(value: -12.0904337), "longitude": NSNumber(value: -77.0188972), "locationName": "Clinica Ricardo Palma"]
            let clinicaTrackable =  Trackable.createTrackable(dicTrackableClinicaRicardo)
            let clinicaBack = UIImage(named: "clinica.jpg")
            let clinicaSpot = UIImage(named: "clinicaSpot.jpg")
            Spot.creatSpot(with: clinicaSpot!, withVideoImage: clinicaBack!, withType: 2, withVideoPath: nil, with: clinicaTrackable, isPublic: false, withUserId: 2)
            
            let dicTrackableBambu:Dictionary<String, Any> = ["fileName": "SpotixDemo", "latitude": NSNumber(value: -12.0899716), "longitude": NSNumber(value: -77.0164991), "locationName": "Edificios Bambu"]
            Trackable.createTrackable(dicTrackableBambu)
            
            let dicTrackableCasa:Dictionary<String, Any> = ["fileName": "SpotixDemo", "latitude": NSNumber(value: -12.0901717), "longitude": NSNumber(value: -77.0170172), "locationName": "Casa Moderna"]
            let casaTrackable = Trackable.createTrackable(dicTrackableCasa)
            let casaBack = UIImage(named: "casaBack.jpg")
            let casaSpot = UIImage(named: "casaSpot.png")
            Spot.creatSpot(with: casaSpot!, withVideoImage: casaBack!, withType: 2, withVideoPath: nil, with: casaTrackable, isPublic: true, withUserId: 3)
        }
        return true
    }
    func applicationWillResignActive(_ application:UIApplication){
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application:UIApplication){
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if let glResourceFinal = glResource {
            glResourceFinal.freeOpenGLESResources()
            glResourceFinal.finishOpenGLESCommands()
        }
    }
    func applicationWillEnterForeground(_ application:UIApplication){
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application:UIApplication){
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application:UIApplication){
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    //MARK: - Core Data
    class func getMOC() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.managedObjectContext
    }
    lazy var applicationDocumentsDirectory:URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count - 1]
    }()
    lazy var managedObjectModel:NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "Spotix", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    lazy var persistentStoreCoordinator:NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do{
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true])
        }catch{
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    lazy var managedObjectContext:NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    func saveContext(){
        if managedObjectContext.hasChanges {
            do{
                try managedObjectContext.save()
            }catch{
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}
