//
//  ImageTargetViewController.h
//  Spotix
//
//  Created by Victor Salazar on 14/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
#import <UIKit/UIKit.h>
@class Trackable;
typedef NS_ENUM(NSInteger, ImageTargetResource){
    ImageTargetResourceNote = 0,
    ImageTargetResourceImage,
    ImageTargetResourceVideo
};
@interface ImageTargetViewController:UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
@property(nonatomic) ImageTargetResource imageTargetResource;
-(Trackable *)getActualTrackable;
@end
