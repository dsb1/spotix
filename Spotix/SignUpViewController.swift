//
//  SignUpViewController.swift
//  Spotix
//
//  Created by victor salazar on 7/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class SignUpViewController:UIViewController{
    //MARK: - IBOutlet
    @IBOutlet weak var userNameTxtFld:UITextField!
    @IBOutlet weak var passwordTxtFld:UITextField!
    @IBOutlet weak var repeatPasswordTxtFld:UITextField!
    @IBOutlet weak var emailTxtFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
    }
    //MARK: - IBAction
    @IBAction func back(){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissKeyboard(){
        self.userNameTxtFld.resignFirstResponder()
        self.passwordTxtFld.resignFirstResponder()
        self.repeatPasswordTxtFld.resignFirstResponder()
        self.emailTxtFld.resignFirstResponder()
    }
}
