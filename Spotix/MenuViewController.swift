//
//  MenuViewController.swift
//  Spotix
//
//  Created by Victor Salazar on 13/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class MenuViewController:UIViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Constants
    let arrOptions = [MainOption(nBackHexColor: "", nImage: "PlacedElements", nTitle: "Placed Elements"), MainOption(nBackHexColor: "", nImage: "MapOfPlaces", nTitle: "Map of Places"), MainOption(nBackHexColor: "", nImage: "DiscoverElements", nTitle: "Discover Elements")]
    var userMainViewCont:UserMainViewController!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated:Bool){
        userMainViewCont = self.parent as! UserMainViewController
    }
    //MARK: - IBAction
    @IBAction func hideMenu(){
        if let viewCont = parent as? UserMainViewController {
            viewCont.hideMenu()
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrOptions.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuOptionCell", for: indexPath) as! MenuOptionTableViewCell
        let option = arrOptions[(indexPath as NSIndexPath).row]
        cell.optionImgView.image = UIImage(named: option.image)
        cell.optionTitleLbl.text = option.title
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        userMainViewCont.selectOption(option: indexPath.row)
    }
}
