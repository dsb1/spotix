/*===============================================================================
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/
#ifndef __SHADERUTILS_H__
#define __SHADERUTILS_H__
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
namespace SampleApplicationUtils{
    void printMatrix(const float *matrix);
    void checkGlError(const char *operation);
    void setRotationMatrix(float angle, float x, float y, float z, float *nMatrix);
    void translatePoseMatrix(float x, float y, float z, float *nMatrix = NULL);
    void rotatePoseMatrix(float angle, float x, float y, float z, float *nMatrix = NULL);
    void scalePoseMatrix(float x, float y, float z, float *nMatrix = NULL);
    void multiplyMatrix(float *matrixA, float *matrixB, float *matrixC);
    int initShader(GLenum nShaderType, const char* pszSource, const char* pszDefs = NULL);
    void setOrthoMatrix(float nLeft, float nRight, float nBottom, float nTop, float nNear, float nFar, float *nProjMatrix);
    void screenCoordToCameraCoord(int screenX, int screenY, int screenDX, int screenDY, int screenWidth, int screenHeight, int cameraWidth, int cameraHeight, int * cameraX, int* cameraY, int * cameraDX, int * cameraDY);
}
#endif  // __SHADERUTILS_H__
