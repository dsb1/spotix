//
//  StarView.swift
//  RateView
//
//  Created by Victor Salazar on 15/10/15.
//  Copyright © 2015 Victor Salazar. All rights reserved.
//
import UIKit
class StarView:UIView{
    var value:CGFloat = 1.0
    var borderColor = UIColor.black
    var backColor = UIColor.white
    var fillColor = UIColor(red: 227/255, green: 158/255, blue: 15/255, alpha: 1.0)
    override init(frame: CGRect){
        super.init(frame:frame)
        self.backgroundColor = UIColor.clear
    }
    required init?(coder aDecoder: NSCoder){
        super.init(coder:aDecoder)
        self.backgroundColor = UIColor.clear
    }
    override func draw(_ rect: CGRect){
        let context = UIGraphicsGetCurrentContext()
        let arm = rect.height
        let path = CGMutablePath()
        path.move(to: CGPoint(x: arm * 0, y: arm * 0.35))
        path.addLine(to: CGPoint(x: arm * 0.35, y: arm * 0.35))
        path.addLine(to: CGPoint(x: arm * 0.5, y: arm * 0.0))
        path.addLine(to: CGPoint(x: arm * 0.65, y: arm * 0.35))
        path.addLine(to: CGPoint(x: arm * 1, y: arm * 0.35))
        path.addLine(to: CGPoint(x: arm * 0.75, y: arm * 0.6))
        path.addLine(to: CGPoint(x: arm * 0.85, y: arm * 1))
        path.addLine(to: CGPoint(x: arm * 0.5, y: arm * 0.75))
        path.addLine(to: CGPoint(x: arm * 0.15, y: arm * 1))
        path.addLine(to: CGPoint(x: arm * 0.25, y: arm * 0.6))
        path.addLine(to: CGPoint(x: arm * 0, y: arm * 0.35))
        context?.saveGState()
        
        let pathCopy = path.copy()
        context?.addPath(pathCopy!)
        context?.setLineWidth(1)
        context?.setStrokeColor(borderColor.cgColor)
        context?.strokePath()
        context?.addPath(path)
        context?.clip()
        context?.setFillColor(backColor.cgColor)
        context?.fill(rect)
        let source = CGPoint(x: 0, y: arm / 2)
        let destination = CGPoint(x: self.value*arm, y: arm/2)
        context?.setStrokeColor(fillColor.cgColor)
        context?.setLineWidth(arm)
        context?.beginPath()
        context?.move(to: source)
        context?.addLine(to: destination)
        context?.closePath()
        context?.strokePath()
        context?.restoreGState()
    }
}
