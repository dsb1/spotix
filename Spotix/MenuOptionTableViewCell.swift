//
//  MenuOptionTableViewCell.swift
//  Spotix
//
//  Created by Victor Salazar on 13/09/16.
//  Copyright © 2016 dsb. All rights reserved.
//
import UIKit
class MenuOptionTableViewCell:UITableViewCell{
    @IBOutlet weak var optionImgView:UIImageView!
    @IBOutlet weak var optionTitleLbl:UILabel!
}