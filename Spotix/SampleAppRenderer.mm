/*===============================================================================
 Copyright (c) 2016 PTC Inc. All Rights Reserved.
 
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/
#import "SampleAppRenderer.h"
#import <UIKit/UIKit.h>
#import <Vuforia/UIGLViewProtocol.h>
#import <Vuforia/Renderer.h>
#import <Vuforia/CameraDevice.h>
#import <Vuforia/Vuforia.h>
#import <Vuforia/TrackerManager.h>
#import <Vuforia/Tool.h>
#import <Vuforia/ObjectTracker.h>
#import <Vuforia/Vuforia.h>
#import <Vuforia/TrackerManager.h>
#import <Vuforia/State.h>
#import <Vuforia/Tool.h>
#import <Vuforia/ObjectTracker.h>
#import <Vuforia/RotationalDeviceTracker.h>
#import <Vuforia/StateUpdater.h>
#import <Vuforia/Renderer.h>
#import <Vuforia/GLRenderer.h>
#import "Texture.h"
#import "SampleApplicationUtils.h"
#import "SampleApplicationShaderUtils.h"
@interface SampleAppRenderer()
@property(nonatomic, readwrite) Vuforia::Device::MODE deviceMode;
@property(nonatomic, readwrite) bool stereo;
@property(nonatomic, assign) id control;
@property(nonatomic, readwrite) GLuint vbShaderProgramID;
@property(nonatomic, readwrite) GLint vbVertexHandle;
@property(nonatomic, readwrite) GLint vbTexCoordHandle;
@property(nonatomic, readwrite) GLint vbTexSampler2DHandle;
@property(nonatomic, readwrite) GLint vbProjectionMatrixHandle;
@property(nonatomic, readwrite) CGFloat nearPlane;
@property(nonatomic, readwrite) CGFloat farPlane;
@property(nonatomic, readwrite) Vuforia::VIEW currentView;
@end
@implementation SampleAppRenderer
-(id)initWithSampleAppRendererControl:(id<SampleAppRendererControl>)control deviceMode:(Vuforia::Device::MODE)deviceMode stereo:(bool)stereo{
    if(self = [super init]){
        self.control = control;
        self.stereo = stereo;
        self.deviceMode = deviceMode;
        self.nearPlane = 50.0f;
        self.farPlane = 5000.0f;
        Vuforia::Device& device = Vuforia::Device::getInstance();
        if (!device.setMode(self.deviceMode)) {
            NSLog(@"ERROR: failed to set the device mode");
        };
        device.setViewerActive(self.stereo);
    }
    return self;
}
-(void)initRendering{
    self.vbShaderProgramID = [SampleApplicationShaderUtils createProgramWithVertexShaderFileName:@"Background.vertsh" fragmentShaderFileName:@"Background.fragsh"];
    if(self.vbShaderProgramID > 0){
        self.vbVertexHandle = glGetAttribLocation(self.vbShaderProgramID, "vertexPosition");
        self.vbTexCoordHandle = glGetAttribLocation(self.vbShaderProgramID, "vertexTexCoord");
        self.vbProjectionMatrixHandle = glGetUniformLocation(self.vbShaderProgramID, "projectionMatrix");
        self.vbTexSampler2DHandle = glGetUniformLocation(self.vbShaderProgramID, "texSampler2D");
    }else{
        NSLog(@"Could not initialise video background shader");
    }
}
-(void)setNearPlane:(CGFloat)near farPlane:(CGFloat)far{
    self.nearPlane = near;
    self.farPlane = far;
}
-(void)renderFrameVuforia{
    Vuforia::Renderer& mRenderer = Vuforia::Renderer::getInstance();
    const Vuforia::State state = Vuforia::TrackerManager::getInstance().getStateUpdater().updateState();
    mRenderer.begin(state);
    
    const Vuforia::RenderingPrimitives renderingPrimitives = Vuforia::Device::getInstance().getRenderingPrimitives();
    Vuforia::ViewList& viewList = renderingPrimitives.getRenderingViews();
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    
    for(int viewIdx = 0; viewIdx < viewList.getNumViews(); viewIdx++){
        Vuforia::VIEW vw = viewList.getView(viewIdx);
        self.currentView = vw;
        
        Vuforia::Vec4I viewport;
        viewport = renderingPrimitives.getViewport(vw);
        
        glViewport(viewport.data[0], viewport.data[1], viewport.data[2], viewport.data[3]);
        
        glScissor(viewport.data[0], viewport.data[1], viewport.data[2], viewport.data[3]);
        
        Vuforia::Matrix34F projMatrix = renderingPrimitives.getProjectionMatrix(vw, Vuforia::COORDINATE_SYSTEM_CAMERA);
        
        Vuforia::Matrix44F rawProjectionMatrixGL = Vuforia::Tool::convertPerspectiveProjection2GLMatrix(projMatrix, self.nearPlane, self.farPlane);
        Vuforia::Matrix44F eyeAdjustmentGL = Vuforia::Tool::convert2GLMatrix(renderingPrimitives.getEyeDisplayAdjustmentMatrix(vw));
        
        Vuforia::Matrix44F projectionMatrix;
        SampleApplicationUtils::multiplyMatrix(&rawProjectionMatrixGL.data[0], &eyeAdjustmentGL.data[0], &projectionMatrix.data[0]);
        if(self.currentView != Vuforia::VIEW_POSTPROCESS){
            [self.control renderFrameWithState:state projectMatrix:projectionMatrix];
        }
        glDisable(GL_SCISSOR_TEST);
    }
    mRenderer.end();
}
-(void)renderVideoBackground{
    if(self.currentView == Vuforia::VIEW_POSTPROCESS){
        return;
    }
    int vbVideoTextureUnit = 0;
    
    Vuforia::GLTextureUnit tex;
    tex.mTextureUnit = vbVideoTextureUnit;
    
    if(!Vuforia::Renderer::getInstance().updateVideoBackgroundTexture(&tex))    {
        NSLog(@"Unable to bind video background texture!!");
        return;
    }
    const Vuforia::RenderingPrimitives renderingPrimitives = Vuforia::Device::getInstance().getRenderingPrimitives();
    
    Vuforia::Matrix44F vbProjectionMatrix = Vuforia::Tool::convert2GLMatrix(renderingPrimitives.getVideoBackgroundProjectionMatrix(self.currentView, Vuforia::COORDINATE_SYSTEM_CAMERA));
    
    if(Vuforia::Device::getInstance().isViewerActive()){
        float sceneScaleFactor = [self getSceneScaleFactor];
        SampleApplicationUtils::scalePoseMatrix(sceneScaleFactor, sceneScaleFactor, 1.0f, vbProjectionMatrix.data);
    }
    
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glDisable(GL_SCISSOR_TEST);
    
    const Vuforia::Mesh& vbMesh = renderingPrimitives.getVideoBackgroundMesh(self.currentView);
    glUseProgram(self.vbShaderProgramID);
    glVertexAttribPointer(self.vbVertexHandle, 3, GL_FLOAT, false, 0, vbMesh.getPositionCoordinates());
    glVertexAttribPointer(self.vbTexCoordHandle, 2, GL_FLOAT, false, 0, vbMesh.getUVCoordinates());
    
    glUniform1i(self.vbTexSampler2DHandle, vbVideoTextureUnit);
    
    glEnableVertexAttribArray(self.vbVertexHandle);
    glEnableVertexAttribArray(self.vbTexCoordHandle);
    
    glUniformMatrix4fv(self.vbProjectionMatrixHandle, 1, GL_FALSE, vbProjectionMatrix.data);
    
    glDrawElements(GL_TRIANGLES, vbMesh.getNumTriangles() * 3, GL_UNSIGNED_SHORT, vbMesh.getTriangles());
    
    glDisableVertexAttribArray(self.vbVertexHandle);
    glDisableVertexAttribArray(self.vbTexCoordHandle);
    SampleApplicationUtils::checkGlError("Rendering of the video background failed");
}
-(float)getSceneScaleFactor{
    static const float VIRTUAL_FOV_Y_DEGS = 85.0f;
    Vuforia::Vec2F fovVector = Vuforia::CameraDevice::getInstance().getCameraCalibration().getFieldOfViewRads();
    float cameraFovYRads = fovVector.data[1];
    float virtualFovYRads = VIRTUAL_FOV_Y_DEGS * M_PI / 180;
    return tan(cameraFovYRads / 2) / tan(virtualFovYRads / 2);
}
@end
